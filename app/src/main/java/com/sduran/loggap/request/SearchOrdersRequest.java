package com.sduran.loggap.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.sduran.loggap.R;
import com.sduran.loggap.support.dto.rest.SearchOrdersRequestDTO;
import com.sduran.loggap.support.dto.rest.SearchOrdersResponseDTO;


public class SearchOrdersRequest extends SpringAndroidSpiceRequest<SearchOrdersResponseDTO> {

    private SearchOrdersRequestDTO SearchOrdersrequest;
    private Resources resources;

    public SearchOrdersRequest(SearchOrdersRequestDTO request, Resources resources) {
        super(SearchOrdersResponseDTO.class);
        this.SearchOrdersrequest = request;
        this.resources = resources;
    }

    @Override
    public SearchOrdersResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint) + resources.getString(R.string.resource_orders_search) ;
        SearchOrdersResponseDTO response = getRestTemplate().postForObject(url, SearchOrdersrequest, SearchOrdersResponseDTO.class);
        return response;
    }
}
