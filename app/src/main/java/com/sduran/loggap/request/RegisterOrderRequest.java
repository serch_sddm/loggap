package com.sduran.loggap.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.sduran.loggap.R;
import com.sduran.loggap.support.dto.rest.RegisterOrderRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterOrderResponseDTO;
import com.sduran.loggap.support.dto.rest.RegisterTravelRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterTravelResponseDTO;


public class RegisterOrderRequest extends SpringAndroidSpiceRequest<RegisterOrderResponseDTO> {

    private RegisterOrderRequestDTO registerOrderRequest;
    private Resources resources;

    public RegisterOrderRequest(RegisterOrderRequestDTO request, Resources resources) {
        super(RegisterOrderResponseDTO.class);
        this.registerOrderRequest = request;
        this.resources = resources;
    }

    @Override
    public RegisterOrderResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint) + resources.getString(R.string.resource_orders_insert);
        RegisterOrderResponseDTO response = getRestTemplate().postForObject(url, registerOrderRequest, RegisterOrderResponseDTO.class);
        return response;
    }
}
