package com.sduran.loggap.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.sduran.loggap.R;
import com.sduran.loggap.support.dto.rest.RegisterCardRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterCardResponseDTO;
import com.sduran.loggap.support.dto.rest.RegisterRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterResponseDTO;


public class RegisterCardRequest extends SpringAndroidSpiceRequest<RegisterCardResponseDTO> {

    private RegisterCardRequestDTO registerCardRequest;
    private Resources resources;

    public RegisterCardRequest(RegisterCardRequestDTO request, Resources resources) {
        super(RegisterCardResponseDTO.class);
        this.registerCardRequest = request;
        this.resources = resources;
    }

    @Override
    public RegisterCardResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint) + resources.getString(R.string.resource_card_insert);
        RegisterCardResponseDTO response = getRestTemplate().postForObject(url, registerCardRequest, RegisterCardResponseDTO.class);
        return response;
    }
}
