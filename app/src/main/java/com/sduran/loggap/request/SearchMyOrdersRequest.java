package com.sduran.loggap.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.sduran.loggap.R;
import com.sduran.loggap.support.dto.rest.SearchMyOrdersRequestDTO;
import com.sduran.loggap.support.dto.rest.SearchMyOrdersResponseDTO;
import com.sduran.loggap.support.dto.rest.SearchOrdersRequestDTO;
import com.sduran.loggap.support.dto.rest.SearchOrdersResponseDTO;


public class SearchMyOrdersRequest extends SpringAndroidSpiceRequest<SearchMyOrdersResponseDTO> {

    private SearchMyOrdersRequestDTO SearchMyOrdersrequest;
    private Resources resources;

    public SearchMyOrdersRequest(SearchMyOrdersRequestDTO request, Resources resources) {
        super(SearchMyOrdersResponseDTO.class);
        this.SearchMyOrdersrequest = request;
        this.resources = resources;
    }

    @Override
    public SearchMyOrdersResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint) + resources.getString(R.string.resource_orders_my_orders) ;
        SearchMyOrdersResponseDTO response = getRestTemplate().postForObject(url, SearchMyOrdersrequest, SearchMyOrdersResponseDTO.class);
        return response;
    }
}
