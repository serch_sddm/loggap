package com.sduran.loggap.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.sduran.loggap.R;
import com.sduran.loggap.support.dto.rest.RegisterRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterResponseDTO;
import com.sduran.loggap.support.dto.rest.RegisterTravelRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterTravelResponseDTO;


public class RegisterTravelRequest extends SpringAndroidSpiceRequest<RegisterTravelResponseDTO> {

    private RegisterTravelRequestDTO registerTravelRequest;
    private Resources resources;

    public RegisterTravelRequest(RegisterTravelRequestDTO request, Resources resources) {
        super(RegisterTravelResponseDTO.class);
        this.registerTravelRequest = request;
        this.resources = resources;
    }

    @Override
    public RegisterTravelResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint) + resources.getString(R.string.resource_travel_insert);
        RegisterTravelResponseDTO response = getRestTemplate().postForObject(url, registerTravelRequest, RegisterTravelResponseDTO.class);
        return response;
    }
}
