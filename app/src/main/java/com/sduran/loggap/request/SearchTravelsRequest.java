package com.sduran.loggap.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.sduran.loggap.R;
import com.sduran.loggap.support.dto.rest.SearchTravelsRequestDTO;
import com.sduran.loggap.support.dto.rest.SearchTravelsResponseDTO;


public class SearchTravelsRequest extends SpringAndroidSpiceRequest<SearchTravelsResponseDTO> {

    private SearchTravelsRequestDTO SearchTravelsrequest;
    private Resources resources;

    public SearchTravelsRequest(SearchTravelsRequestDTO request, Resources resources) {
        super(SearchTravelsResponseDTO.class);
        this.SearchTravelsrequest = request;
        this.resources = resources;
    }

    @Override
    public SearchTravelsResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint) + resources.getString(R.string.resource_travel_search) ;
        SearchTravelsResponseDTO response = getRestTemplate().postForObject(url, SearchTravelsrequest, SearchTravelsResponseDTO.class);
        return response;
    }
}
