package com.sduran.loggap.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.sduran.loggap.R;
import com.sduran.loggap.support.dto.rest.SearchMyTravelsRequestDTO;
import com.sduran.loggap.support.dto.rest.SearchMyTravelsResponseDTO;
import com.sduran.loggap.support.dto.rest.SearchTravelsRequestDTO;
import com.sduran.loggap.support.dto.rest.SearchTravelsResponseDTO;


public class SearchMyTravelsRequest extends SpringAndroidSpiceRequest<SearchMyTravelsResponseDTO> {

    private SearchMyTravelsRequestDTO SearchMyTravelsrequest;
    private Resources resources;

    public SearchMyTravelsRequest(SearchMyTravelsRequestDTO request, Resources resources) {
        super(SearchMyTravelsResponseDTO.class);
        this.SearchMyTravelsrequest = request;
        this.resources = resources;
    }

    @Override
    public SearchMyTravelsResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint) + resources.getString(R.string.resource_travel_my_travels) ;
        SearchMyTravelsResponseDTO response = getRestTemplate().postForObject(url, SearchMyTravelsrequest, SearchMyTravelsResponseDTO.class);
        return response;
    }
}
