package com.sduran.loggap.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.sduran.loggap.R;
import com.sduran.loggap.support.dto.rest.LoginRequestDTO;
import com.sduran.loggap.support.dto.rest.LoginResponseDTO;


public class GetMyOrdersRequest extends SpringAndroidSpiceRequest<LoginResponseDTO> {

    private LoginRequestDTO loginRequest;
    private Resources resources;

    public GetMyOrdersRequest(LoginRequestDTO request, Resources resources) {
        super(LoginResponseDTO.class);
        this.loginRequest = request;
        this.resources = resources;
    }

    @Override
    public LoginResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint) + resources.getString(R.string.resource_user_login);
        LoginResponseDTO response = getRestTemplate().postForObject(url, loginRequest, LoginResponseDTO.class);
        return response;
    }
}
