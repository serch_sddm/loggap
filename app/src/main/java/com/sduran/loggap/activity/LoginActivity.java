package com.sduran.loggap.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sduran.loggap.R;
import com.sduran.loggap.auth.AuthenticationManager;
import com.facebook.FacebookSdk;
import com.sduran.loggap.request.AuthenticationRequest;
import com.sduran.loggap.request.RegisterRequest;
import com.sduran.loggap.support.dto.rest.LoginRequestDTO;
import com.sduran.loggap.support.dto.rest.LoginResponseDTO;
import com.sduran.loggap.support.dto.rest.RegisterRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterResponseDTO;
import com.sduran.loggap.support.dto.rest.UserInfoDTO;
import com.sduran.loggap.util.AlertDialogUtils;
import com.sduran.loggap.util.UIHelper;

import org.json.JSONObject;

import java.util.Arrays;

import static com.sduran.loggap.support.dto.rest.BaseResponseDTO.REST_SUCCESS_CODE;
import static com.sduran.loggap.constants.KeycodeConstants.FACEBOOK_LOGIN_KEY;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private Toolbar toolbar;
    LinearLayout progressView;
    RelativeLayout bodyView;

    private LoginButton loginFacebookButton;
    private CallbackManager callbackManager;

    private ProfileTracker profileTracker;

    String email;
    String password;

    EditText mEmail;
    EditText mPassword;

    UserInfoDTO mInfo;

    String facebookToken = null;
    boolean isFacebookLogin = false;

    String username;

    public static final String KEY_USERNAME = "KEY_USERNAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);
        setToolbar();


        mEmail = (EditText) findViewById(R.id.login_username);
        mPassword = (EditText) findViewById(R.id.login_password);


        if (getIntent().hasExtra(KEY_USERNAME)) {
            mEmail.setText(getIntent().getStringExtra(KEY_USERNAME));
        }

        ((Button) findViewById(R.id.login_loggap_button)).setOnClickListener(this);
        progressView = (LinearLayout) findViewById(R.id.login_progress_form);
        bodyView = (RelativeLayout) findViewById(R.id.login_options);

        loginFacebookButton = (LoginButton) findViewById(R.id.login_facebook_button);

        loginFacebookButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_hometown, user_friends, user_location"));
        loginFacebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                showProgress(true);

                Log.d(MY_TAG, "User ID: " + loginResult.getAccessToken().getUserId() + "\nAuth Token: " + loginResult.getAccessToken().getToken());


                final String faceUsername = loginResult.getAccessToken().getUserId();
                final String facePassword = loginResult.getAccessToken().getUserId();
                facebookToken = loginResult.getAccessToken().getToken();
                isFacebookLogin = true;

                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code
                                /*Log.d(MY_TAG, response.toString());
                                Log.d(MY_TAG, object.toString());*/



                                username = object.optString("name");
                                email = object.optString("email");
                                String faceName = object.optString("first_name");
                                String faceCountry = "México";
                                String faceLastName = object.optString("last_name");

                                attemptRegister(faceCountry,faceUsername,faceLastName,faceName,facePassword);
                                attemptLogin(faceUsername, facePassword);
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,first_name,last_name,hometown,locale,location");
                request.setParameters(parameters);
                request.executeAsync();


            }

            @Override
            public void onCancel() {
                Log.d("Result Facebook: ", "Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException e) {
                Log.d("Result Facebook: ", "Login attempt failed.");
            }
        });
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(true);
            ab.setIcon(R.mipmap.ic_launcher_white);
            ab.setDisplayShowTitleEnabled(false);
            ab.setHomeButtonEnabled(true);
            ab.setDisplayHomeAsUpEnabled(true);
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getTitle());
    }


    private static final String MY_TAG = "ActivityTAG";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(MY_TAG, "Login RequestCode: " + requestCode + "  ResultCode: " + resultCode);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_loggap_button:
                showProgress(true);
                email = mEmail.getText().toString();
                password = mPassword.getText().toString();
                username = email;
                facebookToken = "NA";
                attemptLogin(email, password);
                break;
        }
    }

    private void attemptLogin(String username, String password) {
        AuthenticationRequest request = new AuthenticationRequest(new LoginRequestDTO(username, password), getResources());
        getSpiceManager().execute(request, new AuthenticationRequestListener());
    }

    public void showProgress(final boolean show) {
        UIHelper.showProgress(show, bodyView, progressView, getResources());
    }

    AlertDialog.Builder alertView;

    private final class AuthenticationRequestListener implements RequestListener<LoginResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) { //login response is unsuccessful

            showProgress(false); //hides the waiting progress dialog

            //displays a feedback message on the screen
            alertView = AlertDialogUtils.initAlertDialog(LoginActivity.this, getString(R.string.feedback_warning), "Error: " + spiceException.getMessage(), R.drawable.ic_dialog_alert_holo_light);
            alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertView.show();
        }

        @Override
        public void onRequestSuccess(LoginResponseDTO result) { //login response is successful

            if (REST_SUCCESS_CODE == result.getCode()) { //gets a correct code

                mInfo = result.getUser_info();

                AuthenticationManager.authenticate(LoginActivity.this, facebookToken, result.getToken(), username); //stores the authentication tokens for this session.
                AuthenticationManager.persistUserInfo(LoginActivity.this, mInfo.getName(), mInfo.getLast_name(), mInfo.getCountry(), email);

                Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
                myIntent.putExtra(FACEBOOK_LOGIN_KEY, isFacebookLogin);
                myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myIntent);
                showProgress(false); // hide the waiting progress dialog
                finish();
            } else {  //gets an incorrect code

                showProgress(false); // hide the loading progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(LoginActivity.this, getString(R.string.feedback_warning), getString(R.string.error_invalid_credentials), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertView.show();
            }
        }
    }

    private void attemptRegister(String country, String email, String lastName, String name, String password) {

        RegisterRequest request = new RegisterRequest(new RegisterRequestDTO(country, email, lastName, name, password), getResources());
        getSpiceManager().execute(request, new RegisterRequestListener());

    }

    private final class RegisterRequestListener implements RequestListener<RegisterResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) { //login response is unsuccessful

            showProgress(false); //hides the waiting progress dialog

        }

        @Override
        public void onRequestSuccess(RegisterResponseDTO result) { //login response is successful

            if (REST_SUCCESS_CODE == result.getCode()) { //gets a correct code

                showProgress(false);

            } else {  //gets an incorrect code

                showProgress(false); // hide the loading progress dialog

            }
        }
    }

}
