package com.sduran.loggap.activity;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;


/**
 *
 *
 * Base Activity performing specific RoboSpice stuff
 *
 */
public class BaseActivity extends AppCompatActivity {



    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }


    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }

    private SpiceManager spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);


}
