package com.sduran.loggap.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sduran.loggap.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.facebook.FacebookSdk;
import com.sduran.loggap.auth.AuthenticationManager;
import com.sduran.loggap.request.AuthenticationRequest;
import com.sduran.loggap.request.RegisterRequest;
import com.sduran.loggap.support.dto.rest.LoginRequestDTO;
import com.sduran.loggap.support.dto.rest.LoginResponseDTO;
import com.sduran.loggap.support.dto.rest.RegisterRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterResponseDTO;
import com.sduran.loggap.util.AlertDialogUtils;
import com.sduran.loggap.util.UIHelper;

import static com.sduran.loggap.constants.KeycodeConstants.KEY_CARD_CVV;
import static com.sduran.loggap.constants.KeycodeConstants.KEY_CARD_MONTH;
import static com.sduran.loggap.constants.KeycodeConstants.KEY_CARD_YEAR;
import static com.sduran.loggap.constants.KeycodeConstants.KEY_CARD_NUMBER;
import static com.sduran.loggap.support.dto.rest.BaseResponseDTO.REST_SUCCESS_CODE;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_PAYMENT_INFO = 233;
    private Toolbar toolbar;
    private CallbackManager callbackManager;
    private LoginButton loginFacebookButton;
    public static final String KEY_USERNAME = "KEY_USERNAME";
    public static final String KEY_PASSWORD = "KEY_PASSWORD";

    String mCardNumber;
    String mCardYear;
    String mCardMonth;
    String mCardCVV;

    String mName;
    String mLastName;
    String mEmail;
    String mCountry;
    String mPassword;

    Button mRegisterBtn;

    ImageButton mProfilePhoto;

    private static final int FILECHOOSER_RESULTCODE = 2888;
    //private static final int RESULT_IMAGE_PICKER = 9000;
    private ValueCallback<Uri> mUploadMessage;
    private Uri mCapturedImageURI = null;

    LinearLayout progressView;
    ScrollView bodyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_register);
        setToolbar();

        mRegisterBtn = (Button) findViewById(R.id.button_register);
        mRegisterBtn.setOnClickListener(this);

        loginFacebookButton = (LoginButton) findViewById(R.id.login_facebook_button);
        loginFacebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("Result Facebook: ", "User ID: " + loginResult.getAccessToken().getUserId() + "\nAuth Token: " + loginResult.getAccessToken().getToken());
                authenticate(loginResult.getAccessToken().getToken(), "dummy_token", loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {
                Log.d("Result Facebook: ", "Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException e) {
                Log.d("Result Facebook: ", "Login attempt failed.");
            }
        });

        ArrayAdapter<String> mCountryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, readTitlesRaw());
        AutoCompleteTextView mCountries = (AutoCompleteTextView) findViewById(R.id.country_auto_complete);
        mCountries.setAdapter(mCountryAdapter);
        mCountries.setThreshold(1);

        mProfilePhoto = (ImageButton) findViewById(R.id.profile_image);
        mProfilePhoto.setOnClickListener(this);

        progressView = (LinearLayout) findViewById(R.id.register_progress_form);
        bodyView = (ScrollView) findViewById(R.id.scroll_body);
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(true);
            ab.setIcon(R.mipmap.ic_launcher_white);
            ab.setDisplayShowTitleEnabled(false);
            ab.setHomeButtonEnabled(true);
            ab.setDisplayHomeAsUpEnabled(true);
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getTitle());
    }

    public void authenticate(String fToken, String lToken, String uToken) {

        AuthenticationManager.authenticate(this, fToken, lToken, uToken);
        Intent myIntent = new Intent(RegisterActivity.this, MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myIntent);
        finish();
    }

    private ArrayList<String> readTitlesRaw() {
        BufferedReader reader = null;

        InputStream stream = this.getResources().openRawResource(R.raw.countries);

        reader = new BufferedReader(new InputStreamReader(stream));

        ArrayList<String> mCountries = new ArrayList<String>();
        String line;
        int i = 0;
        try {
            while ((line = reader.readLine()) != null) {

                mCountries.add(line);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return mCountries;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_register:
                showProgress(true);
                setProfilePhoto();
                mName = ((EditText) findViewById(R.id.name)).getText().toString();
                mLastName = ((EditText) findViewById(R.id.last_name)).getText().toString();
                mEmail = ((EditText) findViewById(R.id.email)).getText().toString();
                mCountry = ((EditText) findViewById(R.id.country_auto_complete)).getText().toString();
                mPassword = ((EditText) findViewById(R.id.password)).getText().toString();
                attemptRegister();
                break;
            case R.id.profile_image:
                attemptPhoto();
                break;

        }
    }

    private void setProfilePhoto() {

        AuthenticationManager.persistPhoto(getApplicationContext(), mCapturedImageURI);
    }

    private void attemptPhoto() {
        try {

            // Create AndroidExampleFolder at sdcard

            File imageStorageDir = new File(
                    Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES)
                    , "Loggap");

            if (!imageStorageDir.exists()) {
                // Create AndroidExampleFolder at sdcard
                imageStorageDir.mkdirs();
            }

            // Create camera captured image file path and name
            File file = new File(
                    imageStorageDir + File.separator + "IMG_"
                            + String.valueOf(System.currentTimeMillis())
                            + ".jpg");

            mCapturedImageURI = Uri.fromFile(file);

            // Camera capture image intent
            final Intent captureIntent = new Intent(
                    android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);

            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");

            // Create file chooser intent
            Intent chooserIntent = Intent.createChooser(i, getResources().getString(R.string.choose_image));

            // Set camera intent to file chooser
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
                    , new Parcelable[]{captureIntent});

            // On select image call onActivityResult method of activity
            startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Exception:" + e,
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent intent) {

        Log.d(MY_TAG, "Register RequestCode: " + requestCode + "  ResultCode: " + resultCode);

        if (requestCode == FILECHOOSER_RESULTCODE) {

            /*if (null == this.mUploadMessage) {
                return;

            }*/

            Log.d(MY_TAG, "Enter filechooser ");

            Uri result = null;

            try {

                Log.d(MY_TAG, "Enter try ");

                if (resultCode != Activity.RESULT_OK) {
                    Log.d(MY_TAG, "Enter no ok");
                    result = null;

                } else {

                    Log.d(MY_TAG, "Enter ok ");
                    // retrieve from the private variable if the intent is null
                    result = intent == null ? mCapturedImageURI : intent.getData();
                    Log.d(MY_TAG, "result: " + result);
                    mProfilePhoto.setImageURI(result);

                    mCapturedImageURI = result;


                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "activity :" + e,
                        Toast.LENGTH_LONG).show();
            }

         /*   mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;*/

        } else if (requestCode == REQUEST_CODE_PAYMENT_INFO && resultCode == Activity.RESULT_OK) {
            /*mCardNumber = intent.getStringExtra(KEY_CARD_NUMBER);
            mCardMonth = intent.getStringExtra(KEY_CARD_MONTH);
            mCardYear = intent.getStringExtra(KEY_CARD_YEAR);
            mCardCVV = intent.getStringExtra(KEY_CARD_CVV);

            Log.d(MY_TAG, mCardNumber + "-" + mCardYear + "-" + mCardMonth + "-" + mCardCVV);*/

            Intent myIntent = new Intent(RegisterActivity.this, LoginActivity.class);
            myIntent.putExtra(KEY_USERNAME, mEmail);
            startActivity(myIntent);
            finish();




        } else if (requestCode == REQUEST_CODE_PAYMENT_INFO && resultCode == Activity.RESULT_FIRST_USER) {

            /*mRegisterBtn.setVisibility(View.VISIBLE);
            mPreRegisterBtn.setVisibility(View.GONE);

            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);

            p.addRule(RelativeLayout.ABOVE, R.id.button_post_register);
            p.addRule(RelativeLayout.BELOW, R.id.toolbar_line);

            ((ScrollView) findViewById(R.id.scroll_body)).setLayoutParams(p);*/

            Intent myIntent = new Intent(RegisterActivity.this, LoginActivity.class);
            myIntent.putExtra(KEY_USERNAME, mEmail);
            startActivity(myIntent);
            finish();

        } else if (requestCode == 64206) {
            callbackManager.onActivityResult(requestCode, resultCode, intent);
        } else {

            Log.d(MY_TAG, "Enter final else ");
            super.onActivityResult(requestCode, resultCode, intent);
        }


    }

    public void showProgress(final boolean show) {
        UIHelper.showProgress(show, bodyView, progressView, getResources());
    }

    private void attemptRegister() {

        RegisterRequest request = new RegisterRequest(new RegisterRequestDTO(mCountry, mEmail, mLastName, mName, mPassword), getResources());
        getSpiceManager().execute(request, new RegisterRequestListener());

    }

    AlertDialog.Builder alertView;

    private final class RegisterRequestListener implements RequestListener<RegisterResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) { //login response is unsuccessful

            showProgress(false); //hides the waiting progress dialog

            //displays a feedback message on the screen
            alertView = AlertDialogUtils.initAlertDialog(RegisterActivity.this, getString(R.string.feedback_warning), "Error: " + spiceException.getMessage(), R.drawable.ic_dialog_alert_holo_light);
            alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertView.show();
        }

        @Override
        public void onRequestSuccess(RegisterResponseDTO result) { //login response is successful

            if (REST_SUCCESS_CODE == result.getCode()) { //gets a correct code

                attemptLogin(mEmail, mPassword);

            } else {  //gets an incorrect code

                showProgress(false); // hide the loading progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(RegisterActivity.this, getString(R.string.feedback_warning), getString(R.string.error_invalid_register), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertView.show();
            }
        }
    }

    private void attemptLogin(String username, String password) {
        AuthenticationRequest request = new AuthenticationRequest(new LoginRequestDTO(username, password), getResources());
        getSpiceManager().execute(request, new AuthenticationRequestListener());
    }


    private final class AuthenticationRequestListener implements RequestListener<LoginResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) { //login response is unsuccessful

            showProgress(false); //hides the waiting progress dialog

            //displays a feedback message on the screen
            alertView = AlertDialogUtils.initAlertDialog(RegisterActivity.this, getString(R.string.feedback_warning), "Error: " + spiceException.getMessage(), R.drawable.ic_dialog_alert_holo_light);
            alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertView.show();
        }

        @Override
        public void onRequestSuccess(LoginResponseDTO result) { //login response is successful

            if (REST_SUCCESS_CODE == result.getCode()) { //gets a correct code

                final String resultToken = result.getToken();

                showProgress(false); // hide the waiting progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(RegisterActivity.this, getString(R.string.feedback_success_register), getString(R.string.feedback_payment) + "\n\n" + getString(R.string.loggap_message), R.drawable.ic_ok_holo_dark);
                alertView.setPositiveButton(getString(R.string.feedback_continue), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent payIntent = new Intent(RegisterActivity.this, PaymentActivity.class);
                        payIntent.putExtra("TOKEN_KEY", resultToken);
                        startActivityForResult(payIntent, REQUEST_CODE_PAYMENT_INFO);
                        dialog.dismiss();
                    }
                });
                alertView.show();
            } else {  //gets an incorrect code

                showProgress(false); // hide the loading progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(RegisterActivity.this, getString(R.string.feedback_warning), getString(R.string.error_invalid_credentials), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertView.show();
            }
        }
    }


    private static final String MY_TAG = "ActivityTAG";

}
