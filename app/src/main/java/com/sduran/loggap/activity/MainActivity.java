package com.sduran.loggap.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.sduran.loggap.Fragments.BlankFragment;
import com.sduran.loggap.Fragments.SearchFragment;
import com.sduran.loggap.Fragments.TabActionsFragment;
import com.sduran.loggap.Fragments.TabMyActivityFragment;
import com.sduran.loggap.R;
import com.sduran.loggap.Fragments.PlaceholderFragment;
import com.sduran.loggap.adapters.PlaceArrayAdapter;
import com.sduran.loggap.auth.AuthenticationManager;
import com.sduran.loggap.support.dto.rest.UserInfoDTO;

import static com.sduran.loggap.constants.KeycodeConstants.FACEBOOK_LOGIN_KEY;
import static com.sduran.loggap.constants.KeycodeConstants.KEY_USER_INFO;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks{

    private ProfilePictureView profilePictureView;
    private ImageView profilePictureView2;
    private ProfileTracker profileTracker;

    private Toolbar toolbar;
    private TextView toolbarTitle;
    private android.support.v7.app.ActionBarDrawerToggle toggle;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private String drawerTitle;
    private String username;

    private String name;
    private String lastName;
    private String country;
    private String email;

    Uri mPhotoUri;

    boolean isFacebookLogin = false;

    private static final String LOG_TAG = "MainActivity";
    private static final String MY_TAG = "ActivityTAG";

    private static final int GOOGLE_API_CLIENT_ID = 0;
    public GoogleApiClient mGoogleApiClient;
    public PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(MY_TAG, "onCreate");
        FacebookSdk.sdkInitialize(getApplicationContext());

        //checks for a current login session open.
        if (!AuthenticationManager.isAuthenticated(getApplicationContext())) {
            Log.d(MY_TAG, "no authenticated");
            Intent mIntent = new Intent(MainActivity.this, WelcomeActivity.class);
            startActivity(mIntent);
            finish();
        } else {
            Log.d(MY_TAG, "authenticated");

            isFacebookLogin = getIntent().getBooleanExtra(FACEBOOK_LOGIN_KEY, false);

            Log.d(MY_TAG, "isFacebookLogin:"+isFacebookLogin);
            Log.d(MY_TAG, "getFacebookAuthenticationToken:"+AuthenticationManager.getFacebookAuthenticationToken(getApplicationContext()));
            if (isFacebookLogin) {

                Log.d(MY_TAG, "Begin profile tracker");
                profileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                        updateUI();
                        // It's possible that we were waiting for Profile to be populated in order to
                        // post a status update.
                        //handlePendingAction();
                    }
                };
            }

            setContentView(R.layout.activity_main);
            setToolbar();

            name = AuthenticationManager.getName(getApplicationContext());
            lastName = AuthenticationManager.getLastName(getApplicationContext());
            country = AuthenticationManager.getCountry(getApplicationContext());
            email = AuthenticationManager.getEmail(getApplicationContext());

            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            profilePictureView = (ProfilePictureView) findViewById(R.id.profilePicture);
            profilePictureView2 = (ImageView) findViewById(R.id.profilePicture2);

            if (navigationView != null) {
                setupDrawerContent(navigationView);
            }

            drawerTitle = getResources().getString(R.string.search_item);
            // Check whether we're not recreating a previously destroyed instance
            if (savedInstanceState == null) {
                selectItem(R.id.nav_search, drawerTitle);
                navigationView.getMenu().getItem(3).setChecked(true);
            }

            toggle = new android.support.v7.app.ActionBarDrawerToggle(
                    this, drawerLayout, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
                public void onDrawerOpened(View drawerView) {
                    toolbarTitle.setText(getTitle());
                    //((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getTitle());
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu
                /*Usa este método si vas a modificar la action bar
                con cada fragmento*/
                    //invalidateOptionsMenu();
                }

                public void onDrawerClosed(View drawerView) {
                    toolbarTitle.setText(drawerTitle);
                    //((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(drawerTitle);
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu
                }
            };

            drawerLayout.setDrawerListener(toggle);

        /*toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.ic_drawer);*/

            toggle.syncState();


            //google autocomplete textView
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Places.GEO_DATA_API)
                    .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                    .addConnectionCallbacks(this)
                    .build();

            mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                    BOUNDS_MOUNTAIN_VIEW, null);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        updateUI();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isFacebookLogin) {
                Log.d(MY_TAG, "Stop Profile Tracker!");
                profileTracker.stopTracking();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.stopAutoManage(this);
    }

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        //mPlaceArrayAdapterDestinity.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        //mPlaceArrayAdapterDestinity.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(MY_TAG, "RequestCode: " + requestCode + "  ResultCode: " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void updateUI() {

        if (isFacebookLogin) {
            Log.d(MY_TAG, "Get Profile");
            boolean enableButtons = AccessToken.getCurrentAccessToken() != null;
            Profile profile = Profile.getCurrentProfile();

            if (enableButtons && profile != null) {
                Log.d(MY_TAG, "profile!=null");
                profilePictureView.setVisibility(View.VISIBLE);
                profilePictureView2.setVisibility(View.GONE);
                profilePictureView.setProfileId(profile.getId());
            }
        } else {
            Log.d(MY_TAG, "No facebook login");
            //profilePictureView.setProfileId(null);
            profilePictureView.setVisibility(View.GONE);

            if (!(AuthenticationManager.getPhoto(getApplicationContext()) == null)) {
                mPhotoUri = Uri.parse(AuthenticationManager.getPhoto(getApplicationContext()));
                profilePictureView2.setImageURI(mPhotoUri);
            }


            profilePictureView2.setVisibility(View.VISIBLE);
        }

        username = email;
        ((TextView) findViewById(R.id.username)).setText(username);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);

        final android.support.v7.app.ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.ic_drawer);
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setHomeButtonEnabled(true);
            ab.setIcon(R.mipmap.ic_launcher_white);
            ab.setDisplayShowTitleEnabled(false);
        }

    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        // Marcar item presionado
                        menuItem.setChecked(true);

                        // Crear nuevo fragmento
                        String title = menuItem.getTitle().toString();
                        drawerTitle = title;

                        selectItem(menuItem.getItemId(), title);
                        return true;


                    }
                }
        );
    }

    public void executeLogOut() {

        Log.d(MY_TAG, "Executing logout");

        //clear the authentication session tokens and perform the log out process
        if (isFacebookLogin) {
            Log.d(MY_TAG, "Login manager logout");
            LoginManager.getInstance().logOut();
        }
        AuthenticationManager.logout(this);
        Intent logOutIntent = new Intent(getApplicationContext(), WelcomeActivity.class);
        logOutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(logOutIntent);
        MainActivity.this.finish();
    }

    private void selectItem(int itemId, String titleId) {

        Log.d(MY_TAG, "enter selectItem");
        // Enviar título como arguemento del fragmento
        Bundle args = new Bundle();
        args.putString(PlaceholderFragment.ARG_SECTION_TITLE, titleId);

        android.support.v4.app.Fragment mFragment = null;


        switch (itemId) {
            case R.id.nav_my_activity:
                Log.d(MY_TAG, "enter my activity");
                mFragment = TabMyActivityFragment.newInstance(titleId, 0);
                mFragment.setArguments(args);

                break;
            case R.id.nav_post_trip:
                Log.d(MY_TAG, "enter post trip");
                mFragment = TabActionsFragment.newInstance(titleId, 0);
                //mFragment.setArguments(args);
                break;
            case R.id.nav_get_product:
                Log.d(MY_TAG, "enter get product");
                mFragment = TabActionsFragment.newInstance(titleId, 1);
                //mFragment.setArguments(args);
                break;
            case R.id.nav_search:
                Log.d(MY_TAG, "enter search ejele");
                mFragment = SearchFragment.newInstance(titleId, titleId);
                mFragment.setArguments(args);
                break;
            case R.id.nav_reviews:
                Log.d(MY_TAG, "enter reviews");
                mFragment = BlankFragment.newInstance(titleId, titleId);
                //mFragment.setArguments(args);
                break;
            case R.id.nav_messages:
                Log.d(MY_TAG, "enter messages");
                mFragment = BlankFragment.newInstance(titleId, titleId);
                //mFragment.setArguments(args);
                break;
            case R.id.nav_profile:
                Log.d(MY_TAG, "enter profile");
                mFragment = BlankFragment.newInstance(titleId, titleId);
                //mFragment.setArguments(args);
                break;
            case R.id.nav_payment:
                Log.d(MY_TAG, "enter payment");
                mFragment = BlankFragment.newInstance(titleId, titleId);
                //mFragment.setArguments(args);
                break;
            case R.id.nav_help:
                Log.d(MY_TAG, "enter help");
                mFragment = BlankFragment.newInstance(titleId, titleId);
                //mFragment.setArguments(args);
                break;
            case R.id.nav_log_out:
                Log.d(MY_TAG, "enter logout");
                executeLogOut();
                return;
            default:
                break;

        }

        Log.d(MY_TAG, "After case");

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_content, mFragment)
                .commit();

        drawerLayout.closeDrawers(); // Cerrar drawer

        //setTitle(title); // Setear título actual
        toolbarTitle.setText(titleId);
        //((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(titleId);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);*/
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                else if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }
}
