package com.sduran.loggap.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sduran.loggap.R;
import com.sduran.loggap.auth.AuthenticationManager;
import com.sduran.loggap.request.AuthenticationRequest;
import com.sduran.loggap.request.RegisterCardRequest;
import com.sduran.loggap.request.RegisterRequest;
import com.sduran.loggap.support.dto.rest.LoginRequestDTO;
import com.sduran.loggap.support.dto.rest.LoginResponseDTO;
import com.sduran.loggap.support.dto.rest.RegisterCardRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterCardResponseDTO;
import com.sduran.loggap.support.dto.rest.RegisterRequestDTO;
import com.sduran.loggap.util.AlertDialogUtils;
import com.sduran.loggap.util.UIHelper;

import static com.sduran.loggap.constants.KeycodeConstants.KEY_CARD_CVV;
import static com.sduran.loggap.constants.KeycodeConstants.KEY_CARD_MONTH;
import static com.sduran.loggap.constants.KeycodeConstants.KEY_CARD_YEAR;
import static com.sduran.loggap.constants.KeycodeConstants.KEY_CARD_NUMBER;
import static com.sduran.loggap.support.dto.rest.BaseResponseDTO.REST_SUCCESS_CODE;


public class PaymentActivity extends BaseActivity implements View.OnClickListener {

    private Toolbar toolbar;

    LinearLayout progressView;
    LinearLayout bodyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        setToolbar();
        ((Button) findViewById(R.id.confirm_payment_btn)).setOnClickListener(this);

        progressView = (LinearLayout) findViewById(R.id.register_progress_form);
        bodyView = (LinearLayout) findViewById(R.id.payment_options);

        mToken = getIntent().getStringExtra("TOKEN_KEY");
    }


    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(true);
            ab.setIcon(R.mipmap.ic_launcher_white);
            ab.setDisplayShowTitleEnabled(false);
            ab.setHomeButtonEnabled(true);
            ab.setDisplayHomeAsUpEnabled(false);
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getTitle());
    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_payment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_skip) {
            Intent returnIntent = new Intent();
            setResult(RESULT_FIRST_USER, returnIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showProgress(final boolean show) {
        UIHelper.showProgress(show, bodyView, progressView, getResources());
    }

    String cardYear, cardMonth, cardNumber, cardCVV, mToken;

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.confirm_payment_btn) {
            showProgress(true);

            cardYear= ((EditText) findViewById(R.id.year)).getText().toString();
            cardMonth=((EditText) findViewById(R.id.month)).getText().toString();
            cardNumber=((EditText) findViewById(R.id.card_number)).getText().toString();
            cardCVV=((EditText) findViewById(R.id.cvv)).getText().toString();

            attemptRegisterCard();
        }
    }

    private void attemptRegisterCard() {

        RegisterCardRequest request = new RegisterCardRequest(new RegisterCardRequestDTO("NA",cardCVV,cardMonth,cardYear,cardNumber,mToken), getResources());
        getSpiceManager().execute(request, new RegisterCardRequestListener());
    }


    AlertDialog.Builder alertView;


    private final class RegisterCardRequestListener implements RequestListener<RegisterCardResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) { //login response is unsuccessful

            showProgress(false); //hides the waiting progress dialog

            //displays a feedback message on the screen
            alertView = AlertDialogUtils.initAlertDialog(PaymentActivity.this, getString(R.string.feedback_warning), "Error: " + spiceException.getMessage(), R.drawable.ic_dialog_alert_holo_light);
            alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertView.show();
        }

        @Override
        public void onRequestSuccess(RegisterCardResponseDTO result) { //login response is successful

            if (REST_SUCCESS_CODE == result.getCode()) { //gets a correct code
                showProgress(false); // hide the waiting progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(PaymentActivity.this, getString(R.string.feedback_success_register_card1), getString(R.string.feedback_success_register_card), R.drawable.ic_ok_holo_dark);
                alertView.setPositiveButton(getString(R.string.feedback_end), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent returnIntent = new Intent();
                        setResult(RESULT_OK, returnIntent);
                        finish();
                        dialog.dismiss();
                    }
                });
                alertView.show();
            } else {  //gets an incorrect code

                showProgress(false); // hide the loading progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(PaymentActivity.this, getString(R.string.feedback_warning), getString(R.string.error_invalid_card), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertView.show();
            }
        }
    }


}
