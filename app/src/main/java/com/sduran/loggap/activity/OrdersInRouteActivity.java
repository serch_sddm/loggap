package com.sduran.loggap.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sduran.loggap.R;
import com.sduran.loggap.adapters.OrdersArrayAdapter;
import com.sduran.loggap.auth.AuthenticationManager;
import com.sduran.loggap.request.SearchOrdersRequest;
import com.sduran.loggap.support.dto.rest.LoginResponseDTO;
import com.sduran.loggap.support.dto.rest.OrdersDTO;
import com.sduran.loggap.support.dto.rest.SearchOrdersRequestDTO;
import com.sduran.loggap.support.dto.rest.SearchOrdersResponseDTO;
import com.sduran.loggap.util.AlertDialogUtils;
import com.sduran.loggap.util.UIHelper;

import java.util.List;

import static com.sduran.loggap.support.dto.rest.BaseResponseDTO.REST_EXPIRED_CODE;
import static com.sduran.loggap.support.dto.rest.BaseResponseDTO.REST_SUCCESS_CODE;

public class OrdersInRouteActivity extends BaseActivity {

    private Toolbar toolbar;

    String destinityPlace;
    String originPlace;
    String destinityDate;
    String originDate;

    LinearLayout progressView;
    ListView bodyView;
    TextView mFeedback;


    private static final String MY_TAG = "ActivityTAG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_in_route);

        setToolbar();


        progressView = (LinearLayout) findViewById(R.id.orders_route_progress_form);
        bodyView = (ListView) findViewById(R.id.my_listview);
        mFeedback = (TextView) findViewById(R.id.feedback_message);

        bodyView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO: Edit the grade entry via a dialog.
            }
        });

        destinityPlace = getIntent().getStringExtra("DESTINITY_PLACE");
        originPlace = getIntent().getStringExtra("ORIGIN_PLACE");
        destinityDate = getIntent().getStringExtra("DESTINITY_DATE");
        originDate = getIntent().getStringExtra("ORIGIN_DATE");


        ((TextView) findViewById(R.id.header_orders_in_route)).setText(originPlace+" -> " +destinityPlace);

        Log.d(MY_TAG,originPlace+"-"+destinityPlace+"-"+originDate+"-"+originPlace);

        showProgress(true);

        attemptSearch();

    }

    private void attemptSearch() {
        /*SearchOrdersRequestDTO requestDTO = new SearchOrdersRequestDTO();
        requestDTO.setDestiny_date(destinityDate);
        requestDTO.setDestiny_place(destinityPlace);
        requestDTO.setOrigin_date(originDate);
        requestDTO.setOrigin_place(originPlace);
        requestDTO.setToken(AuthenticationManager.getRESTAuthenticationToken(getApplicationContext()));*/

        String token = AuthenticationManager.getRESTAuthenticationToken(getApplicationContext());

        Log.d(MY_TAG,"token:"+token);

        SearchOrdersRequest request = new SearchOrdersRequest(new SearchOrdersRequestDTO(destinityDate,destinityPlace,originDate,originPlace,token), getResources());
        getSpiceManager().execute(request, new SearchOrdersRequestListener());
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(true);
            ab.setIcon(R.mipmap.ic_launcher_white);
            ab.setDisplayShowTitleEnabled(false);
            ab.setHomeButtonEnabled(true);
            ab.setDisplayHomeAsUpEnabled(true);
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getTitle());
    }

    public void showProgress(final boolean show) {
        UIHelper.showProgress(show, bodyView, progressView, getResources());
    }

    AlertDialog.Builder alertView;
    OrdersArrayAdapter adapter;

    // Search fines
    private class SearchOrdersRequestListener implements RequestListener<SearchOrdersResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e("Error ", spiceException.getMessage());
            showProgress(false);
            /*Toast.makeText(getActivity(),
                    getString(R.string.error_tax_not_found), Toast.LENGTH_SHORT)
                    .show();
*/
            alertView = AlertDialogUtils.initAlertDialog(getApplicationContext(),getString(R.string.title_orders_incorrect), getString(R.string.description_orders_not_found), R.drawable.ic_dialog_alert_holo_light);
            alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertView.show();
        }

        @Override
        public void onRequestSuccess(SearchOrdersResponseDTO response) {

            showProgress(false);

            if(REST_SUCCESS_CODE == response.getCode()){

                //gets and sends the concepts retrieved from details
                List<OrdersDTO> items;
                items = response.getOrders();


                adapter = new OrdersArrayAdapter(OrdersInRouteActivity.this,
                        android.R.layout.simple_list_item_1, items);
                bodyView.setAdapter(adapter);


               /* for(OrdersDTO item: items){

                    Log.d(MY_TAG,"order found");
                }*/

                //finish();


             /*   if(response.getStatusWS().equals("001")) {

                    Intent detailIntent = new Intent(getActivity(), TaxDetailActivity.class);
                    detailIntent.putExtra(KeycodeConstants.TAX_PARAM_KEY, response);
                    getActivity().startActivity(detailIntent);
                    getActivity().finish();
                }
                else{

                    alertView = AlertDialogUtils.initAlertDialog(getActivity(),getString(R.string.title_reference_incorrect), response.getMessageWS(), R.drawable.ic_dialog_alert_holo_light);
                    alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertView.show();
                }*/

            }else if (REST_EXPIRED_CODE == response.getCode()) { //token expired
                showProgress(false); // hide the loading progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(OrdersInRouteActivity.this, getString(R.string.feedback_warning), getString(R.string.error_token_expired)+""+getString(R.string.feedback_logout), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();


                    }
                });
                alertView.show();

            }else{
                Log.d("Error ", response.getMessage());

                /*Toast.makeText(getActivity(),
                        getString(R.string.error_tax_not_found), Toast.LENGTH_SHORT)
                        .show();*/

                alertView = AlertDialogUtils.initAlertDialog(OrdersInRouteActivity.this,getString(R.string.title_orders_incorrect), getString(R.string.description_orders_not_found), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertView.show();
                mFeedback.setVisibility(View.VISIBLE);
                bodyView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_orders_in_route, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
