package com.sduran.loggap.constants;

/**
 * Created by LMARTINEZ on 06/01/2015.
 */
public final class SystemConstants {

    public final static String DATABASE_NAME = "oax_movil2";

    public final static String FACEBOOK_TOKEN_KEY = "FACEBOOK_TOKEN_KEY";
    public final static String REST_TOKEN_KEY = "REST_TOKEN_KEY";
    public final static String USERNAME_KEY = "USERNAME_KEY";
    public final static String PHOTO_KEY = "PHOTO_KEY";
    public final static String NAME_KEY = "NAME_KEY";
    public final static String LAST_NAME_KEY = "LAST_NAME_KEY";
    public final static String COUNTRY_KEY = "COUNTRY_KEY";
    public final static String EMAIL_KEY = "EMAIL_KEY";
}
