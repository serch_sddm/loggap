package com.sduran.loggap.constants;

/**
 * Created by LMARTINEZ on 25/11/2014.
 */
public final class KeycodeConstants {

    //implement this line just for testing interface.
    public final static boolean FAST_TEST = false; //flag to control how to test the interface query's, if you want to load the default success values for the API's(make "true" this flag) or if you want to  write manually this values (make "false" this flag).

    public final static String PREFS = "PREFS";
    public final static String REST_TOKEN_KEY = "authentication_token";
    public final static String ADQUIRA_TOKEN_KEY = "adquira_authentication_token";
    public final static String USERNAME_KEY = "username_key";

    public final static String NUMERO_CUENTA_KEY = "numeroCuenta_key";
    public final static String TIPO_PREDIO_KEY = "tipoPredio_key";
    public final static String PROPIETARIO_ESCRITURA_KEY = "propietarioEscritura_key";
    public final static String SALDO_ULTIMO_CORTE_KEY = "saldoUltimoCorte_key";
    public final static String DOMICILIO_FISCAL_KEY = "domicilioFiscal_key";
    public final static String DESCONTADO_KEY = "descontado_key";
    public final static String GENERA_VALE_KEY = "generaVale_key";
    public final static String DETAILS_KEY = "details_key";
    public final static String ELEMENTS_KEY = "elements_key";

    public final static String NUMBER_DETAILS_KEY = "number_details_key" ;
    public final static String NUMBER_ELEMENTS_KEY = "number_elements_key" ;
    /*public final static String CONCEPT_KEY = "concept_key" ;
    public final static String CONCEPT_NAME_KEY = "concept_name_key" ;
    public final static String CONCEPT_AMOUNT_KEY = "concept_amount_key" ;
    public final static String CONCEPT_DESCOUNT_KEY = "concept_descount_key" ;
    public final static String CONCEPT_TOTAL_KEY = "concept_total_key" ;*/

    public final static String LOG_OUT_KEY          = "finish";

    public final static String NAME_ACTIVITY_KEY = "name_activity_key";

    public static final String KEY_CARD_NUMBER = "KEY_NUM_BUTTONS";
    public static final String KEY_CARD_MONTH = "KEY_CARD_MONTH";
    public static final String KEY_CARD_YEAR = "KEY_CARD_YEAR";
    public static final String KEY_CARD_CVV = "KEY_CARD_CVV";

    public static final String KEY_USER_INFO = "KEY_USER_INFO";

    public static final String FACEBOOK_LOGIN_KEY = "FACEBOOK_LOGIN_KEY";

}
