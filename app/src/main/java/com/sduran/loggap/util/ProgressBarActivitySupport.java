package com.sduran.loggap.util;

public interface ProgressBarActivitySupport {

    public void showProgress(boolean show);
}
