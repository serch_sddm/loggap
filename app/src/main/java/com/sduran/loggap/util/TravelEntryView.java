package com.sduran.loggap.util;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sduran.loggap.R;


public class TravelEntryView extends RelativeLayout {

    private TextView nameTextView;
    private TextView weightTextView;
    private TextView destinyDateTextView;
    private TextView originDateTextView;
    private RatingBar ratingRatingBar;


    public TravelEntryView(Context context) {
        super(context);
        ((Activity) context).getLayoutInflater().inflate(R.layout.traveler_entry_view, this);
        nameTextView = (TextView) findViewById(R.id.traveler_entry_name);
        weightTextView = (TextView) findViewById(R.id.traveler_entry_weight);
        destinyDateTextView = (TextView) findViewById(R.id.traveler_entry_destiny_date);
        originDateTextView = (TextView) findViewById(R.id.traveler_entry_origin_date);
        ratingRatingBar = (RatingBar) findViewById(R.id.traveler_entry_rating);
    }


    public void setName(String name) {
        nameTextView.setText(name);
    }

    public void setRating(int rating){

        ratingRatingBar.setRating(rating);
    }

    public void setWeight(int weigth) {
        weightTextView.setText(String.valueOf(weigth));
    }

    public void setOriginDate(String origin) {
        originDateTextView.setText(origin);
    }

    public void setDestinyDate(String destiny) {
        destinyDateTextView.setText(destiny);
    }


}
