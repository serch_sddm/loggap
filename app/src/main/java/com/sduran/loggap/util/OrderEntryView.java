package com.sduran.loggap.util;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sduran.loggap.R;

import org.w3c.dom.Text;


public class OrderEntryView extends RelativeLayout {

    private TextView nameTextView;
    private TextView costTextView;
    private TextView volumeTextView;
    private TextView weightTextView;
    private TextView destinyDateTextView;
    private TextView originDateTextView;


    public OrderEntryView(Context context) {
        super(context);
        ((Activity) context).getLayoutInflater().inflate(R.layout.order_entry_view, this);
        nameTextView = (TextView) findViewById(R.id.order_entry_name);
        costTextView = (TextView) findViewById(R.id.order_entry_cost);
        volumeTextView = (TextView) findViewById(R.id.order_entry_volume);
        weightTextView = (TextView) findViewById(R.id.order_entry_weight);
        destinyDateTextView = (TextView) findViewById(R.id.order_entry_destiny_date);
        originDateTextView = (TextView) findViewById(R.id.order_entry_origin_date);
    }


    public void setName(String name) {
        nameTextView.setText(name);
    }

    public void setCost(Double cost) {
        costTextView.setText("$" + String.valueOf(cost));
    }

    public void setVolume(String volume) {
        volumeTextView.setText(volume);
    }

    public void setWeight(int weigth) {
        weightTextView.setText(String.valueOf(weigth));
    }

    public void setOriginDate(String origin) {
        originDateTextView.setText(origin);
    }

    public void setDestinyDate(String destiny) {
        destinyDateTextView.setText(destiny);
    }


}
