package com.sduran.loggap.auth;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import static com.sduran.loggap.constants.SystemConstants.FACEBOOK_TOKEN_KEY;
import static com.sduran.loggap.constants.SystemConstants.REST_TOKEN_KEY;
import static com.sduran.loggap.constants.SystemConstants.USERNAME_KEY;
import static com.sduran.loggap.constants.SystemConstants.PHOTO_KEY;
import static com.sduran.loggap.constants.SystemConstants.NAME_KEY;
import static com.sduran.loggap.constants.SystemConstants.LAST_NAME_KEY;
import static com.sduran.loggap.constants.SystemConstants.COUNTRY_KEY;
import static com.sduran.loggap.constants.SystemConstants.EMAIL_KEY;

public final class AuthenticationManager {

    public static final String PREFS = "PREFS";
   public static boolean isAuthenticated(Context context){
       return !TextUtils.isEmpty(getFacebookAuthenticationToken(context))
               && !TextUtils.isEmpty(getRESTAuthenticationToken(context));
   }

    public static String getRESTAuthenticationToken(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(REST_TOKEN_KEY, null);
    }

    public static String getFacebookAuthenticationToken(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(FACEBOOK_TOKEN_KEY, null);
    }

    public static String getUsername(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(USERNAME_KEY, null);
    }

    public static void authenticate(Context context, String facebookToken, String RESTToken, String username){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(FACEBOOK_TOKEN_KEY, facebookToken);
        editor.putString(REST_TOKEN_KEY, RESTToken);
        editor.putString(USERNAME_KEY, username);
        editor.commit();
    }

    public static void logout(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(FACEBOOK_TOKEN_KEY);
        editor.remove(REST_TOKEN_KEY);
        editor.remove(USERNAME_KEY);
        editor.commit();
    }

    //USER INFO

    public static void persistPhoto(Context context, Uri mUri){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PHOTO_KEY, mUri.toString());
        editor.commit();
    }

    public static String getPhoto(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PHOTO_KEY, null);
    }

    public static void persistUserInfo(Context context, String name, String lastName, String country, String email){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(NAME_KEY, name);
        editor.putString(LAST_NAME_KEY, lastName);
        editor.putString(COUNTRY_KEY, country);
        editor.putString(EMAIL_KEY, email);
        editor.commit();
    }

    public static String getName(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(NAME_KEY, null);
    }

    public static String getLastName(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(LAST_NAME_KEY, null);
    }

    public static String getCountry(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(COUNTRY_KEY, null);
    }


    public static String getEmail(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(EMAIL_KEY, null);
    }
}
