package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class OrdersDTO implements Serializable {

    private String origin_date;
    private String destiny_place;
    private String origin_place;
    private String volume;
    private String destiny_date;
    private int weight;
    private Double cost;
    private String description;
    private String name;


    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrdersDTO() {

    }


    public OrdersDTO(String origin_date, String destiny_place, String origin_place, String volume, String destiny_date, int weight, String name, String description, Double cost) {
        this.origin_date = origin_date;
        this.destiny_place = destiny_place;
        this.origin_place = origin_place;
        this.volume = volume;
        this.destiny_date = destiny_date;
        this.weight = weight;
        this.name = name;
        this.cost = cost;
        this.description = description;
    }


    public String getOrigin_date() {
        return origin_date;
    }

    public void setOrigin_date(String origin_date) {
        this.origin_date = origin_date;
    }

    public String getDestiny_place() {
        return destiny_place;
    }

    public void setDestiny_place(String destiny_place) {
        this.destiny_place = destiny_place;
    }

    public String getOrigin_place() {
        return origin_place;
    }

    public void setOrigin_place(String origin_place) {
        this.origin_place = origin_place;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getDestiny_date() {
        return destiny_date;
    }

    public void setDestiny_date(String destiny_date) {
        this.destiny_date = destiny_date;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int max_weight) {
        this.weight = max_weight;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OrdersDTO{");
        sb.append("origin_date='").append(origin_date).append('\'');
        sb.append(", destiny_place='").append(destiny_place).append('\'');
        sb.append(", origin_place='").append(origin_place).append('\'');
        sb.append(", volume='").append(volume).append('\'');
        sb.append(", destiny_date='").append(destiny_date).append('\'');
        sb.append(", weight=").append(weight);
        sb.append(", cost=").append(cost);
        sb.append(", description='").append(description).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
