package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class SearchTravelsRequestDTO implements Serializable {

    private String destiny_date;
    private String destiny_place;
    private String origin_date;
    private String origin_place;
    private String token;


    public String getDestiny_date() {
        return destiny_date;
    }

    public void setDestiny_date(String destiny_date) {
        this.destiny_date = destiny_date;
    }

    public String getDestiny_place() {
        return destiny_place;
    }

    public void setDestiny_place(String destiny_place) {
        this.destiny_place = destiny_place;
    }

    public String getOrigin_date() {
        return origin_date;
    }

    public void setOrigin_date(String origin_date) {
        this.origin_date = origin_date;
    }

    public String getOrigin_place() {
        return origin_place;
    }

    public void setOrigin_place(String origin_place) {
        this.origin_place = origin_place;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SearchTravelsRequestDTO{");
        sb.append("destiny_date='").append(destiny_date).append('\'');
        sb.append(", destiny_place='").append(destiny_place).append('\'');
        sb.append(", origin_date='").append(origin_date).append('\'');
        sb.append(", origin_place='").append(origin_place).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
