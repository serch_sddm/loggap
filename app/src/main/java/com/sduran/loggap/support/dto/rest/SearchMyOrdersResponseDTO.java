package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class SearchMyOrdersResponseDTO extends BaseResponseDTO implements Serializable {

    private List<OrdersDTO> orders;

    public List<OrdersDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrdersDTO> ordersByItem) {
        List<OrdersDTO> orders = new ArrayList<OrdersDTO>();
        for (OrdersDTO dto : ordersByItem) {
            orders.add(new OrdersDTO(dto.getOrigin_date(), dto.getDestiny_place(), dto.getOrigin_place(), dto.getVolume(), dto.getDestiny_date(), dto.getWeight(),dto.getName(),dto.getDescription(),dto.getCost()));
        }
        this.orders = orders;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SearchMyOrdersResponseDTO{");
        sb.append("code='").append(getCode()).append('\'');
        sb.append(", message='").append(getMessage()).append('\'');
        sb.append("orders=").append(orders);
        sb.append('}');
        return sb.toString();
    }
}
