package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class RegisterOrderRequestDTO implements Serializable {

    private Double cost;
    private String description;
    private String destiny_date;
    private String destiny_place;
    private String name;
    private String origin_date;
    private String origin_place;
    private String token;
    private String volume;
    private int weight;

    public RegisterOrderRequestDTO(Double cost, String description, String destiny_date, String destiny_place, String name, String origin_date, String origin_place, String token, String volume, int weight) {
        this.cost = cost;
        this.description = description;
        this.destiny_date = destiny_date;
        this.destiny_place = destiny_place;
        this.name = name;
        this.origin_date = origin_date;
        this.origin_place = origin_place;
        this.token = token;
        this.volume = volume;
        this.weight = weight;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDestiny_date() {
        return destiny_date;
    }

    public void setDestiny_date(String destiny_date) {
        this.destiny_date = destiny_date;
    }

    public String getDestiny_place() {
        return destiny_place;
    }

    public void setDestiny_place(String destiny_place) {
        this.destiny_place = destiny_place;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin_date() {
        return origin_date;
    }

    public void setOrigin_date(String origin_date) {
        this.origin_date = origin_date;
    }

    public String getOrigin_place() {
        return origin_place;
    }

    public void setOrigin_place(String origin_place) {
        this.origin_place = origin_place;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegisterOrderRequestDTO{");
        sb.append("cost='").append(cost).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", destiny_date='").append(destiny_date).append('\'');
        sb.append(", destiny_place='").append(destiny_place).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", origin_date='").append(origin_date).append('\'');
        sb.append(", origin_place='").append(origin_place).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append(", volume='").append(volume).append('\'');
        sb.append(", weight='").append(weight).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
