package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class RegisterTravelRequestDTO implements Serializable {

    private String destiny_date;
    private String destiny_place;
    private String details;
    private int max_weight;
    private String origin_date;
    private String origin_place;
    private String token;
    private String volume;

    public RegisterTravelRequestDTO(String destiny_date, String destiny_place, String details, int max_weight, String origin_date, String origin_place, String token, String volume) {
        this.destiny_date = destiny_date;
        this.destiny_place = destiny_place;
        this.details = details;
        this.max_weight = max_weight;
        this.origin_date = origin_date;
        this.origin_place = origin_place;
        this.token = token;
        this.volume = volume;
    }

    public String getDestiny_date() {
        return destiny_date;
    }

    public void setDestiny_date(String destiny_date) {
        this.destiny_date = destiny_date;
    }

    public String getDestiny_place() {
        return destiny_place;
    }

    public void setDestiny_place(String destiny_place) {
        this.destiny_place = destiny_place;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getMax_weight() {
        return max_weight;
    }

    public void setMax_weight(int max_weight) {
        this.max_weight = max_weight;
    }

    public String getOrigin_date() {
        return origin_date;
    }

    public void setOrigin_date(String origin_date) {
        this.origin_date = origin_date;
    }

    public String getOrigin_place() {
        return origin_place;
    }

    public void setOrigin_place(String origin_place) {
        this.origin_place = origin_place;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegisterTravelRequestDTO{");
        sb.append("destiny_date='").append(destiny_date).append('\'');
        sb.append(", destiny_place='").append(destiny_place).append('\'');
        sb.append(", details='").append(details).append('\'');
        sb.append(", max_weight='").append(max_weight).append('\'');
        sb.append(", origin_date='").append(origin_date).append('\'');
        sb.append(", origin_place='").append(origin_place).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append(", volume='").append(volume).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
