package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class RegisterCardResponseDTO extends BaseResponseDTO implements Serializable {

    public RegisterCardResponseDTO() {
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegisterCardResponseDTO{");
        sb.append("code='").append(getCode()).append('\'');
        sb.append(", message='").append(getMessage()).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
