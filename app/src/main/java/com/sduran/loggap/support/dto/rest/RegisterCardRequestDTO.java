package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class RegisterCardRequestDTO implements Serializable {

    private String country;
    private String cvv;
    private String expiracy_month;
    private String expiracy_year;
    private String number;
    private String token;

    public RegisterCardRequestDTO(String country, String cvv, String expiracy_month, String expiracy_year, String number, String token) {
        this.country = country;
        this.cvv = cvv;
        this.expiracy_month = expiracy_month;
        this.expiracy_year = expiracy_year;
        this.number = number;
        this.token = token;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getExpiracy_month() {
        return expiracy_month;
    }

    public void setExpiracy_month(String expiracy_month) {
        this.expiracy_month = expiracy_month;
    }

    public String getExpiracy_year() {
        return expiracy_year;
    }

    public void setExpiracy_year(String expiracy_year) {
        this.expiracy_year = expiracy_year;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegisterCardRequestDTO{");
        sb.append("country='").append(country).append('\'');
        sb.append(", cvv='").append(cvv).append('\'');
        sb.append(", expiracy_month='").append(expiracy_month).append('\'');
        sb.append(", expiracy_year='").append(expiracy_year).append('\'');
        sb.append(", number='").append(number).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
