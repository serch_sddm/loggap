package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class SearchTravelsResponseDTO extends BaseResponseDTO implements Serializable {

    private List<TravelsDTO> travels;

    public List<TravelsDTO> getTravels() {
        return travels;
    }

    public void setTravels(List<TravelsDTO> travelsByItem) {
        List<TravelsDTO> travels = new ArrayList<TravelsDTO>();
        for (TravelsDTO dto : travelsByItem) {
            travels.add(new TravelsDTO(dto.getOrigin_date(), dto.getDestiny_place(), dto.getOrigin_place(), dto.getVolume(), dto.getDestiny_date(), dto.getMax_weight()));
        }
        this.travels = travels;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SearchTravelsResponseDTO{");
        sb.append("code='").append(getCode()).append('\'');
        sb.append(", message='").append(getMessage()).append('\'');
        sb.append("travels=").append(travels);
        sb.append('}');
        return sb.toString();
    }
}
