package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class SearchMyTravelsRequestDTO implements Serializable {

    private String token;

    public SearchMyTravelsRequestDTO(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SearchMyTravelsRequestDTO{");
        sb.append("token='").append(token).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
