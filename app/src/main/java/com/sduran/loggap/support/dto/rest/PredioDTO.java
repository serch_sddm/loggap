package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

/**
 * Created by LMARTINEZ on 27/11/2014.
 */
public class PredioDTO implements Serializable {

    private String clave;
    private String concepto;
    private String edo;
    private String importe;
    private String descuento;
    private String importePendienteDePago;
    private String minAnio;

    public PredioDTO() {
    }

    public PredioDTO(String clave, String concepto, String edo, String importe, String descuento, String importePendienteDePago, String minAnio) {
        this.clave = clave;
        this.concepto = concepto;
        this.edo = edo;
        this.importe = importe;
        this.descuento = descuento;
        this.importePendienteDePago = importePendienteDePago;
        this.minAnio = minAnio;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getEdo() {
        return edo;
    }

    public void setEdo(String edo) {
        this.edo = edo;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getDescuento() { return descuento; }

    public void setDescuento (String descuento) { this.descuento = descuento; }

    public String getImportePendienteDePago() {
        return importePendienteDePago;
    }

    public void setImportePendienteDePago(String importePendienteDePago) {
        this.importePendienteDePago = importePendienteDePago;
    }

    public String getMinAnio() {
        return minAnio;
    }

    public void setMinAnio(String minAnio) {
        this.minAnio = minAnio;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(" {");
        sb.append("clave='").append(clave).append('\'');
        sb.append(", concepto='").append(concepto).append('\'');
        sb.append(", edo='").append(edo).append('\'');
        sb.append(", importe='").append(importe).append('\'');
        sb.append(", descuento='").append(descuento).append('\'');
        sb.append(", importePendienteDePago='").append(importePendienteDePago).append('\'');
        sb.append(", minAnio='").append(minAnio).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
