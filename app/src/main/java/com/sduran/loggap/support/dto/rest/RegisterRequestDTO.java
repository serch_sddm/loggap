package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class RegisterRequestDTO implements Serializable {

    private String country;
    private String email;
    private String last_name;
    private String name;
    private String password;


    public RegisterRequestDTO(String country, String email, String last_name, String name, String password) {
        this.country = country;
        this.email = email;
        this.last_name = last_name;
        this.name = name;
        this.password = password;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegisterRequestDTO{");
        sb.append("country='").append(country).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", last_name='").append(last_name).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
