package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class LoginResponseDTO extends BaseResponseDTO implements Serializable {


    private UserInfoDTO user_info;

    private String token;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserInfoDTO getUser_info() {
        return user_info;
    }

    public void setUser_info(UserInfoDTO user_info) {
        this.user_info = user_info;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LoginResponseDTO{");
        sb.append("code='").append(getCode()).append('\'');
        sb.append(", message='").append(getMessage()).append('\'');
        sb.append("user_info=").append(user_info);
        sb.append(", token='").append(token).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
