package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class ConciliationRequestDTO implements Serializable {

    private String numeroCuenta;
    private String tipoPredio;
    private String sectransmision;
    private String token;

    public ConciliationRequestDTO(String numeroCuenta, String tipoPredio, String sectransmision, String token) {
        this.numeroCuenta = numeroCuenta;
        this.tipoPredio = tipoPredio;
        this.sectransmision = sectransmision;
        this.token = token;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public String getTipoPredio() {
        return tipoPredio;
    }

    public String getSectransmision() { return sectransmision; }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ConciliationRequest{");
        sb.append("numeroCuenta='").append(numeroCuenta).append('\'');
        sb.append(", tipoPredio='").append(tipoPredio).append('\'');
        sb.append(", sectransmision='").append(sectransmision).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
