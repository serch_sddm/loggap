package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class RegisterTravelResponseDTO extends BaseResponseDTO implements Serializable {

    public RegisterTravelResponseDTO() {
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegisterTravelResponseDTO{");
        sb.append("code='").append(getCode()).append('\'');
        sb.append(", message='").append(getMessage()).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
