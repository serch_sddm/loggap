package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

/**
 * Created by LMARTINEZ on 27/11/2014.
 */
public class PrediosByNameDTO implements Serializable {

    private String cuentaPredial;
    private String propietario;
    private String domicilioFiscal;
    private String cuenta;
    private String tipo;

    public PrediosByNameDTO() {
    }

    public PrediosByNameDTO(String cuentaPredial, String propietario, String domicilioFiscal, String cuenta, String tipo) {
        this.cuentaPredial = cuentaPredial;
        this.propietario = propietario;
        this.domicilioFiscal = domicilioFiscal;
        this.cuenta = cuenta;
        this.tipo = tipo;
    }

    public String getCuentaPredial() {
        return cuentaPredial;
    }

    public void setCuentaPredial(String cuentaPredial) {
        this.cuentaPredial = cuentaPredial;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getTipo() { return tipo; }

    public void setTipo (String tipo) { this.tipo = tipo; }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(" {");
        sb.append("cuentaPredial='").append(cuentaPredial).append('\'');
        sb.append(", propietario='").append(propietario).append('\'');
        sb.append(", domicilioFiscal='").append(domicilioFiscal).append('\'');
        sb.append(", cuenta='").append(cuenta).append('\'');
        sb.append(", tipo='").append(tipo).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
