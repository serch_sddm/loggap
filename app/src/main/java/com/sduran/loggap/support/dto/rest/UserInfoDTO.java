package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;


public class UserInfoDTO implements Serializable {

    private String country;
    private String last_name;
    private String name;

    public UserInfoDTO() {
    }

    public UserInfoDTO(String country, String last_name, String name) {
        this.country = country;
        this.last_name = last_name;
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserInfoDTO{");
        sb.append("country='").append(country).append('\'');
        sb.append(", last_name='").append(last_name).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
