package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class TravelsDTO implements Serializable {

    private String origin_date;
    private String destiny_place;
    private String origin_place;
    private String volume;
    private String destiny_date;
    private int max_weight;

    public TravelsDTO() {

    }

    public TravelsDTO(String origin_date, String destiny_place, String origin_place, String volume, String destiny_date, int max_weight) {
        this.origin_date = origin_date;
        this.destiny_place = destiny_place;
        this.origin_place = origin_place;
        this.volume = volume;
        this.destiny_date = destiny_date;
        this.max_weight = max_weight;
    }


    public String getOrigin_date() {
        return origin_date;
    }

    public void setOrigin_date(String origin_date) {
        this.origin_date = origin_date;
    }

    public String getDestiny_place() {
        return destiny_place;
    }

    public void setDestiny_place(String destiny_place) {
        this.destiny_place = destiny_place;
    }

    public String getOrigin_place() {
        return origin_place;
    }

    public void setOrigin_place(String origin_place) {
        this.origin_place = origin_place;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getDestiny_date() {
        return destiny_date;
    }

    public void setDestiny_date(String destiny_date) {
        this.destiny_date = destiny_date;
    }

    public int getMax_weight() {
        return max_weight;
    }

    public void setMax_weight(int max_weight) {
        this.max_weight = max_weight;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TravelsDTO{");
        sb.append("origin_date='").append(origin_date).append('\'');
        sb.append(", destiny_place='").append(destiny_place).append('\'');
        sb.append(", origin_place='").append(origin_place).append('\'');
        sb.append(", volume='").append(volume).append('\'');
        sb.append(", destiny_date='").append(destiny_date).append('\'');
        sb.append(", max_weight=").append(max_weight);
        sb.append('}');
        return sb.toString();
    }
}
