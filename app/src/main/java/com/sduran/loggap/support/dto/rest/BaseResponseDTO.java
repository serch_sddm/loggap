package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;


public class BaseResponseDTO implements Serializable {

    public final static int REST_SUCCESS_CODE = 1;
    public final static int REST_INVALID_TOKEN_CODE = -2;
    public final static int REST_EXPIRED_CODE = -1;
    public final static int REST_INVALID_CREDENTIALS = -1;
    public final static int REST_NOT_FOUOND_CODE = 2;
    public final static String REST_SUCCESS_MESSAGE = "Success";
    public final static String REST_ERROR_MESSAGE = "Error";

    private int code;
    private String message;
    private String kind;
    private String etag;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BaseResponseDTO{");
        sb.append("code=").append(code);
        sb.append(", message='").append(message).append('\'');
        sb.append("kind='").append(kind).append('\'');
        sb.append(", etag='").append(etag).append('\'');
        sb.append('}');
        return sb.toString();
    }


}
