package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class RegisterResponseDTO extends BaseResponseDTO implements Serializable {

    public RegisterResponseDTO() {
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegisterResponseDTO{");
        sb.append("code='").append(getCode()).append('\'');
        sb.append(", message='").append(getMessage()).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
