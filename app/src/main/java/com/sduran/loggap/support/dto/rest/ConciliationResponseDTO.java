package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LMARTINEZ on 27/11/2014.
 */
public class ConciliationResponseDTO extends BaseResponseDTO implements Serializable {

    private List<ConciliationDTO> details;

    public List<ConciliationDTO> getDetails() {
        return details;
    }

    public void setDetails(List<ConciliationDTO> details) {
        List<ConciliationDTO> items = new ArrayList<ConciliationDTO>();
        for(ConciliationDTO dto : details){
            items.add(new ConciliationDTO());
        }
        this.details = items;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ConciliationResponseDTO{");
        sb.append("code='").append(getCode()).append('\'');
        sb.append(", message='").append(getMessage()).append('\'');
        sb.append(", details='").append(details).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
