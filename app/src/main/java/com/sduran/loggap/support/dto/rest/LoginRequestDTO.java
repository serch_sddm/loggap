package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class LoginRequestDTO implements Serializable {

    private String email;
    private String password;

    public LoginRequestDTO(String email, String password){
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LoginRequest{");
        sb.append("email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
