package com.sduran.loggap.support.dto.rest;

import java.io.Serializable;

public class RegisterOrderResponseDTO extends BaseResponseDTO implements Serializable {

    public RegisterOrderResponseDTO() {
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegisterOrderResponseDTO{");
        sb.append("code='").append(getCode()).append('\'');
        sb.append(", message='").append(getMessage()).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
