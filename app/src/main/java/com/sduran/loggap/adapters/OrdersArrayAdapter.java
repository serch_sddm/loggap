package com.sduran.loggap.adapters;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.sduran.loggap.support.dto.rest.OrdersDTO;
import com.sduran.loggap.util.OrderEntryView;


public class OrdersArrayAdapter extends ArrayAdapter<OrdersDTO> {

    private Context mContext;

    public OrdersArrayAdapter(Context context, int resource, List<OrdersDTO> orderEntries) {
        super(context, resource, orderEntries);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OrderEntryView view = null;
        if (convertView == null) {
            view = new OrderEntryView(mContext);
        } else {
            view = (OrderEntryView) convertView;
        }
        OrdersDTO orderEntry = getItem(position);

        // CONSIDER: map could be null...
        String description = orderEntry.getDescription();
        String name= orderEntry.getName();
        Double cost = orderEntry.getCost();
        String volume = orderEntry.getVolume();
        int weight = orderEntry.getWeight();
        String destiny_date = orderEntry.getDestiny_date();
        String origin_date = orderEntry.getOrigin_date();

        destiny_date = truncate(destiny_date,10);
        origin_date = truncate(origin_date,10);

        view.setName(name);
        view.setCost(cost); ;
        view.setVolume(volume);
        view.setWeight(weight);
        view.setDestinyDate(destiny_date);
        view.setOriginDate(origin_date);

        return view;
    }

    public static String truncate(String value, int length)
    {
        if (value != null && value.length() > length)
            value = value.substring(0, length);
        return value;
    }

}
