package com.sduran.loggap.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.sduran.loggap.support.dto.rest.OrdersDTO;
import com.sduran.loggap.support.dto.rest.TravelsDTO;
import com.sduran.loggap.util.OrderEntryView;
import com.sduran.loggap.util.TravelEntryView;

import java.util.List;
import java.util.Random;


public class TravelersArrayAdapter extends ArrayAdapter<TravelsDTO> {

    private Context mContext;

    public TravelersArrayAdapter(Context context, int resource, List<TravelsDTO> orderEntries) {
        super(context, resource, orderEntries);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TravelEntryView view = null;
        if (convertView == null) {
            view = new TravelEntryView(mContext);
        } else {
            view = (TravelEntryView) convertView;
        }
        TravelsDTO travelEntry = getItem(position);

        // CONSIDER: map could be null...
        int weight = travelEntry.getMax_weight();
        String destiny_date = travelEntry.getDestiny_date();
        String origin_date = travelEntry.getOrigin_date();

        destiny_date = truncate(destiny_date,10);
        origin_date = truncate(origin_date,10);

        view.setName("Nombre del viajero");
        view.setWeight(weight);
        view.setDestinyDate(destiny_date);
        view.setOriginDate(origin_date);

        Random rand = new Random();
        int randomNum = rand.nextInt((5 - 0) + 1) + 0;

        view.setRating(randomNum);

        return view;
    }

    public static String truncate(String value, int length)
    {
        if (value != null && value.length() > length)
            value = value.substring(0, length);
        return value;
    }

}
