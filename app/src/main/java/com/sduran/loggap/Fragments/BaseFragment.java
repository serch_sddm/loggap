package com.sduran.loggap.Fragments;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.sduran.loggap.util.ProgressBarActivitySupport;




public class BaseFragment extends android.support.v4.app.Fragment {

    @Override
    public void onStart() {
        spiceManager.start(getActivity());
        super.onStart();
    }

    @Override
    public void onStop() {
        if (spiceManager.isStarted()) {
            spiceManager.shouldStop();
        }
        super.onStop();
    }



    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }



    private SpiceManager spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);


    public void showParentActivityProgress(boolean show) {
        try {
            ProgressBarActivitySupport activity = ((ProgressBarActivitySupport)getActivity());
            activity.showProgress(show);
        } catch (ClassCastException e) {
            Log.e("ACTIVITY ERROR","Parent activity should implement ProgressBarActivitySupport");
        }
    }

}
