package com.sduran.loggap.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sduran.loggap.R;
import com.sduran.loggap.activity.BaseActivity;
import com.sduran.loggap.activity.MainActivity;
import com.sduran.loggap.activity.OrdersInRouteActivity;
import com.sduran.loggap.activity.PaymentActivity;
import com.sduran.loggap.activity.RegisterActivity;
import com.sduran.loggap.adapters.PlaceArrayAdapter;
import com.sduran.loggap.auth.AuthenticationManager;
import com.sduran.loggap.request.RegisterRequest;
import com.sduran.loggap.request.RegisterTravelRequest;
import com.sduran.loggap.support.dto.rest.RegisterRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterResponseDTO;
import com.sduran.loggap.support.dto.rest.RegisterTravelRequestDTO;
import com.sduran.loggap.support.dto.rest.RegisterTravelResponseDTO;
import com.sduran.loggap.util.AlertDialogUtils;
import com.sduran.loggap.util.UIHelper;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import static com.sduran.loggap.support.dto.rest.BaseResponseDTO.REST_EXPIRED_CODE;
import static com.sduran.loggap.support.dto.rest.BaseResponseDTO.REST_SUCCESS_CODE;

/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PostTripFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PostTripFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class PostTripFragment extends BaseFragment implements
       /* GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,*/
        DatePickerDialog.OnDateSetListener, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    /*private OnFragmentInteractionListener mListener;*/

    /*
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PostTripFragment.
     */

    private static final String MY_TAG = "ActivityTAG";

    private static final String LOG_TAG = "MainActivity";
    private static final int GOOGLE_API_CLIENT_ID = 1;

    private AutoCompleteTextView mAutoCompFrom;
    private AutoCompleteTextView mAutoCompTo;
    TextView mDateFrom;
    TextView mDateTo;
    Spinner mVolume;
    Spinner mWeight;
    EditText mDetails;

    private String destiny_date;
    private String destiny_place;
    private int max_weight;
    private String origin_date;
    private String origin_place;
    private String token;
    private String volume;
    private String details;

    String[] itemsVolume = new String[]{"xChico", "Chico", "Mediano", "Grande", "xGrande", "xxGrande"};
    String[] itemsWeight = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "15", "20"};

    /*private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;*/
    //private PlaceArrayAdapter mPlaceArrayAdapterDestinity;
    /*private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));*/


    // TODO: Rename and change types and number of parameters
/*    public static PostTripFragment newInstance(String param1, String param2) {
        PostTripFragment fragment = new PostTripFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/
    public static PostTripFragment newInstance() {
        PostTripFragment fragment = new PostTripFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public PostTripFragment() {
        // Required empty public constructor
    }

    /*   @Override
       public void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           setRetainInstance(true);
           if (getArguments() != null) {
               mParam1 = getArguments().getString(ARG_PARAM1);
               mParam2 = getArguments().getString(ARG_PARAM2);
           }
       }*/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
    }

  /*  @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        Log.d(MY_TAG, "enter OnStop");
        super.onStop();
        mGoogleApiClient.stopAutoManage(getActivity());
    }
*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_post_trip, container, false);

       /* mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(getActivity(), GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();*/
        mAutoCompFrom = (AutoCompleteTextView) v.findViewById(R.id
                .autoCompleteTextView_origin);
        mAutoCompTo = (AutoCompleteTextView) v.findViewById(R.id
                .autoCompleteTextView_destinity);
        mAutoCompFrom.setThreshold(3);
        mAutoCompTo.setThreshold(3);
        mAutoCompFrom.setOnItemClickListener(mAutocompleteClickListenerOrigin);
        /*((MainActivity)getActivity()).mPlaceArrayAdapter = new PlaceArrayAdapter(getContext(), android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);*/
        mAutoCompFrom.setAdapter(((MainActivity) getActivity()).mPlaceArrayAdapter);
        mAutoCompTo.setOnItemClickListener(mAutocompleteClickListenerDestinity);
        /*mPlaceArrayAdapterDestinity = new PlaceArrayAdapter(getContext(), android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);*/
        mAutoCompTo.setAdapter(((MainActivity) getActivity()).mPlaceArrayAdapter);

        // Set up the 'clear text' button that clears the text in the autocomplete view
        ImageButton clearButtonOrigin = (ImageButton) v.findViewById(R.id.button_clear_origin);
        clearButtonOrigin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutoCompFrom.setText("");
            }
        });

        ImageButton clearButtonDestinity = (ImageButton) v.findViewById(R.id.button_clear_destinity);
        clearButtonDestinity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutoCompTo.setText("");
            }
        });

        mDateFrom = (TextView) v.findViewById(R.id.date_from);
        mDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        PostTripFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        String date = "" + year + "-" + (++monthOfYear) + "-" + dayOfMonth;
                        mDateFrom.setText(date);


                    }
                });
                dpd.setThemeDark(false);
                dpd.vibrate(true);
                dpd.dismissOnPause(false);
                dpd.showYearPickerFirst(false);
                dpd.setAccentColor(Color.parseColor("#039be5"));
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        mDateTo = (TextView) v.findViewById(R.id.date_to);
        mDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        PostTripFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        String date = "" + year + "-" + (++monthOfYear) + "-" + dayOfMonth;
                        mDateTo.setText(date);


                    }
                });
                dpd.setThemeDark(false);
                dpd.vibrate(true);
                dpd.dismissOnPause(false);
                dpd.showYearPickerFirst(false);
                dpd.setAccentColor(Color.parseColor("#039be5"));
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });


        mWeight = (Spinner) v.findViewById(R.id.max_weight);
        mWeight.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, itemsWeight));
        mVolume = (Spinner) v.findViewById(R.id.max_volume);
        mVolume.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, itemsVolume));

        ((Button) v.findViewById(R.id.search_btn)).setOnClickListener(this);

        progressView = (LinearLayout) v.findViewById(R.id.post_trip_progress_form);
        bodyView = (ScrollView) v.findViewById(R.id.post_container);

        mDetails = (EditText) v.findViewById(R.id.details);

        return v;
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListenerOrigin
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = ((MainActivity) getActivity()).mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(((MainActivity) getActivity()).mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private AdapterView.OnItemClickListener mAutocompleteClickListenerDestinity
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = ((MainActivity) getActivity()).mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(((MainActivity) getActivity()).mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
// Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();

           /* mNameTextView.setText(Html.fromHtml(place.getName() + ""));
            mAddressTextView.setText(Html.fromHtml(place.getAddress() + ""));
            mIdTextView.setText(Html.fromHtml(place.getId() + ""));
            mPhoneTextView.setText(Html.fromHtml(place.getPhoneNumber() + ""));
            mWebTextView.setText(place.getWebsiteUri() + "");
            if (attributions != null) {
                mAttTextView.setText(Html.fromHtml(attributions.toString()));
            }*/
        }
    };

    /*@Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        //mPlaceArrayAdapterDestinity.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(getContext(),
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        //mPlaceArrayAdapterDestinity.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }*/

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int i, int i1, int i2) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.search_btn) {
            showProgress(true);

            destiny_date = mDateTo.getText().toString();
            destiny_place = mAutoCompTo.getText().toString();
            max_weight = Integer.valueOf(itemsWeight[mWeight.getSelectedItemPosition()]);
            origin_date = mDateFrom.getText().toString();
            origin_place = mAutoCompFrom.getText().toString();
            token = AuthenticationManager.getRESTAuthenticationToken(getContext());
            volume = mVolume.getSelectedItem().toString();
            details = mDetails.getText().toString();

            destiny_date = destiny_date + "T00:00:00";
            origin_date = origin_date + "T00:00:00";

            Log.d(MY_TAG, "dd:" + destiny_date + " dp:" + destiny_place + " mw:" + max_weight + " od:" + origin_date + " op" + origin_place + " t: " + token + " v:" + volume + " det:" + details);
            attemptTravelRegister();


        }
    }

    private void attemptTravelRegister() {
        RegisterTravelRequest request = new RegisterTravelRequest(new RegisterTravelRequestDTO(destiny_date, destiny_place, details + destiny_place, max_weight, origin_date, origin_place, token, volume), getResources());
        getSpiceManager().execute(request, new RegisterTravelRequestListener());
    }

    AlertDialog.Builder alertView;

    private final class RegisterTravelRequestListener implements RequestListener<RegisterTravelResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) { //login response is unsuccessful

            showProgress(false); //hides the waiting progress dialog

            //displays a feedback message on the screen
            alertView = AlertDialogUtils.initAlertDialog(getContext(), getString(R.string.feedback_warning), "Error: " + spiceException.getMessage(), R.drawable.ic_dialog_alert_holo_light);
            alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertView.show();
        }

        @Override
        public void onRequestSuccess(RegisterTravelResponseDTO result) { //login response is successful

            if (REST_SUCCESS_CODE == result.getCode()) { //gets a correct code

                showProgress(false);

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(getContext(), getString(R.string.feedback_success_register), getString(R.string.feedback_success_register_travel), R.drawable.ic_ok_holo_dark);
                alertView.setPositiveButton(getString(R.string.feedback_continue), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent mIntent = new Intent(getContext(), OrdersInRouteActivity.class);
                        mIntent.putExtra("DESTINITY_PLACE", destiny_place);
                        mIntent.putExtra("ORIGIN_PLACE", origin_place);
                        mIntent.putExtra("DESTINITY_DATE", destiny_date);
                        mIntent.putExtra("ORIGIN_DATE", origin_date);
                        //mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(mIntent);

                    }
                });
                alertView.show();


            } else if (REST_EXPIRED_CODE == result.getCode()) { //token expired
                showProgress(false); // hide the loading progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(getContext(), getString(R.string.feedback_warning), getString(R.string.error_token_expired)+""+getString(R.string.error_logout), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ((MainActivity)getActivity()).executeLogOut();

                    }
                });
                alertView.show();

            } else {  //gets an incorrect code

                showProgress(false); // hide the loading progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(getContext(), getString(R.string.feedback_warning), getString(R.string.error_invalid_trip), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
                alertView.show();
            }
        }
    }

    LinearLayout progressView;
    ScrollView bodyView;

    public void showProgress(final boolean show) {
        UIHelper.showProgress(show, bodyView, progressView, getResources());
    }

    /*// TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    *//**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *//*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
*/
}
