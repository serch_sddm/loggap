package com.sduran.loggap.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.sduran.loggap.R;
import com.sduran.loggap.activity.MainActivity;
import com.sduran.loggap.activity.OrdersInRouteActivity;
import com.sduran.loggap.activity.TravelersInRouteActivity;
import com.sduran.loggap.adapters.PlaceArrayAdapter;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

/*https://github.com/wdullaer/MaterialDateTimePicker*/

public class SearchFragment extends android.support.v4.app.Fragment implements
        /*GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,*/
        DatePickerDialog.OnDateSetListener, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private static final String MY_TAG = "ActivityTAG";

    private static final String LOG_TAG = "MainActivity";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private AutoCompleteTextView mAutoCompFrom;
    private AutoCompleteTextView mAutoCompTo;
    private TextView mNameTextView;
    private TextView mAddressTextView;
    private TextView mIdTextView;
    private TextView mPhoneTextView;
    private TextView mWebTextView;
    private TextView mAttTextView;
    //private GoogleApiClient mGoogleApiClient;
    //private PlaceArrayAdapter mPlaceArrayAdapter;
    //private PlaceArrayAdapter mPlaceArrayAdapterDestinity;
    /*private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));*/

    TextView dateFrom;
    TextView dateTo;

    int selectedOption;
    RadioGroup mRadioGroup;
    RadioButton mRadioButtton;

    /*private OnFragmentInteractionListener mListener;*/

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
// TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

/*    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.stopAutoManage(getActivity());
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search, container, false);

        ((RadioGroup) v.findViewById(R.id.group_rdb)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.post_rdb) {
                    Log.d(MY_TAG, "post");
                } else if (checkedId == R.id.get_rdb) {
                    Log.d(MY_TAG, "get");
                }
            }
        });

        /*mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(getActivity(), GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();*/
        mAutoCompFrom = (AutoCompleteTextView) v.findViewById(R.id
                .autoCompleteTextView_origin);
        mAutoCompTo = (AutoCompleteTextView) v.findViewById(R.id
                .autoCompleteTextView_destinity);
        mAutoCompFrom.setThreshold(3);
        mAutoCompTo.setThreshold(3);
        mNameTextView = (TextView) v.findViewById(R.id.name);
        mAddressTextView = (TextView) v.findViewById(R.id.address);
        mIdTextView = (TextView) v.findViewById(R.id.place_id);
        mPhoneTextView = (TextView) v.findViewById(R.id.phone);
        mWebTextView = (TextView) v.findViewById(R.id.web);
        mAttTextView = (TextView) v.findViewById(R.id.att);
        mAutoCompFrom.setOnItemClickListener(mAutocompleteClickListenerOrigin);
        /*mPlaceArrayAdapter = new PlaceArrayAdapter(getContext(), android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);*/
       /* ((MainActivity)getActivity()).mPlaceArrayAdapter = new PlaceArrayAdapter(getContext(), android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);*/

        //mAutoCompFrom.setAdapter(mPlaceArrayAdapter);
        mAutoCompFrom.setAdapter(((MainActivity)getActivity()).mPlaceArrayAdapter);
        mAutoCompTo.setOnItemClickListener(mAutocompleteClickListenerDestinity);
        /*mPlaceArrayAdapterDestinity = new PlaceArrayAdapter(getContext(), android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);*/
        mAutoCompTo.setAdapter(((MainActivity)getActivity()).mPlaceArrayAdapter);

        // Set up the 'clear text' button that clears the text in the autocomplete view
        ImageButton clearButtonOrigin = (ImageButton) v.findViewById(R.id.button_clear_origin);
        clearButtonOrigin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutoCompFrom.setText("");
            }
        });

        ImageButton clearButtonDestinity = (ImageButton) v.findViewById(R.id.button_clear_destinity);
        clearButtonDestinity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutoCompTo.setText("");
            }
        });

        dateFrom = (TextView) v.findViewById(R.id.date_from);
        dateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        SearchFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        String date = "" + year + "-" + (++monthOfYear) + "-" + dayOfMonth;
                        dateFrom.setText(date);


                    }
                });
                dpd.setThemeDark(false);
                dpd.vibrate(true);
                dpd.dismissOnPause(false);
                dpd.showYearPickerFirst(false);
                dpd.setAccentColor(Color.parseColor("#039be5"));
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        dateTo = (TextView) v.findViewById(R.id.date_to);
        dateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        SearchFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        String date = "" + year + "-" + (++monthOfYear) + "-" + dayOfMonth;
                        dateTo.setText(date);


                    }
                });
                dpd.setThemeDark(false);
                dpd.vibrate(true);
                dpd.dismissOnPause(false);
                dpd.showYearPickerFirst(false);
                dpd.setAccentColor(Color.parseColor("#039be5"));


                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        ((Button) v.findViewById(R.id.search_btn)).setOnClickListener(this);

        mRadioGroup = (RadioGroup) v.findViewById(R.id.group_rdb);


        return v;
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListenerOrigin
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = ((MainActivity)getActivity()).mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(((MainActivity)getActivity()).mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private AdapterView.OnItemClickListener mAutocompleteClickListenerDestinity
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = ((MainActivity)getActivity()).mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(((MainActivity)getActivity()).mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
// Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();

            mNameTextView.setText(Html.fromHtml(place.getName() + ""));
            mAddressTextView.setText(Html.fromHtml(place.getAddress() + ""));
            mIdTextView.setText(Html.fromHtml(place.getId() + ""));
            mPhoneTextView.setText(Html.fromHtml(place.getPhoneNumber() + ""));
            mWebTextView.setText(place.getWebsiteUri() + "");
            if (attributions != null) {
                mAttTextView.setText(Html.fromHtml(attributions.toString()));
            }
        }
    };

/*    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        //mPlaceArrayAdapterDestinity.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(getContext(),
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        //mPlaceArrayAdapterDestinity.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }*/


    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int i, int i1, int i2) {

    }

    private String origin_place;
    private String destiny_place;

    private String origin_date;
    private String destiny_date;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.search_btn) {


            selectedOption = mRadioGroup.getCheckedRadioButtonId();
            if (selectedOption == R.id.post_rdb) {

                destiny_place = mAutoCompTo.getText().toString();
                origin_place = mAutoCompFrom.getText().toString();

                destiny_date = dateTo.getText().toString();
                origin_date = dateFrom.getText().toString();


                destiny_date = destiny_date + "T00:00:00";
                origin_date = origin_date + "T00:00:00";

                Intent mSearchIntent = new Intent(getContext(), OrdersInRouteActivity.class);

                mSearchIntent.putExtra("DESTINITY_PLACE", destiny_place);
                mSearchIntent.putExtra("ORIGIN_PLACE", origin_place);
                mSearchIntent.putExtra("DESTINITY_DATE", destiny_date);
                mSearchIntent.putExtra("ORIGIN_DATE", origin_date);

                startActivity(mSearchIntent);

            } else if (selectedOption == R.id.get_rdb) {
                destiny_place = mAutoCompTo.getText().toString();
                origin_place = mAutoCompFrom.getText().toString();

                destiny_date = dateTo.getText().toString();
                origin_date = dateFrom.getText().toString();


                destiny_date = destiny_date + "T00:00:00";
                origin_date = origin_date + "T00:00:00";

                Intent mSearchIntent = new Intent(getContext(), TravelersInRouteActivity.class);

                mSearchIntent.putExtra("DESTINITY_PLACE", destiny_place);
                mSearchIntent.putExtra("ORIGIN_PLACE", origin_place);
                mSearchIntent.putExtra("DESTINITY_DATE", destiny_date);
                mSearchIntent.putExtra("ORIGIN_DATE", origin_date);

                startActivity(mSearchIntent);
            }
        }
    }

/*
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
   /* public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }*/


}
