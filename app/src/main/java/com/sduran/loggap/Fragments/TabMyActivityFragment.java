package com.sduran.loggap.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.samples.apps.iosched.ui.widget.SlidingTabLayout;
import com.sduran.loggap.R;

import java.util.Locale;

/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TabMyActivityFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TabMyActivityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabMyActivityFragment extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private int mParam2;

    ViewPager mViewPager;
    SlidingTabLayout slidingTabs;

    private static final String MY_TAG = "ActivityTAG";

    /*private OnFragmentInteractionListener mListener;*/

    /*
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TabFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TabMyActivityFragment newInstance(String param1, int param2) {
        Log.d(MY_TAG, "first mParam1=" + param1);
        Log.d(MY_TAG, "first mParam2=" + param2);
        TabMyActivityFragment fragment = new TabMyActivityFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public TabMyActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getInt(ARG_PARAM2);
            Log.d(MY_TAG, "second mParam1=" + mParam1);
            Log.d(MY_TAG, "second mParam2=" + mParam2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(MY_TAG, "oncreateView");
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tab_my_activity, container, false);

        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        slidingTabs = (SlidingTabLayout) v.findViewById(R.id.sliding_tabs);

        // Set up the ViewPager with the sections adapter.
        Log.d(MY_TAG, "preSectionPager");
        mViewPager.setAdapter(new SectionsPagerAdapter(getChildFragmentManager()));
        Log.d(MY_TAG, "postSectionPager");

        //slidingTabs = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        slidingTabs.setDistributeEvenly(true);
        slidingTabs.setBackgroundColor(getResources().getColor(R.color.smooth_gray));
        slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.accentColor));
   /*     slidingTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);    //define any color in xml resources and set it here, I have used white
            }

            @Override
            public int getDividerColor(int position) {
                return getResources().getColor(R.color.primaryDarkerColor);
            }
        });*/
        slidingTabs.setViewPager(mViewPager);

        Log.d(MY_TAG, "mParam2=" + mParam2);
        mViewPager.setCurrentItem(mParam2);
        Log.d(MY_TAG, "postSetCurrentItem");

        return v;
    }

  /*  // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }*/

    private class SectionsPagerAdapter extends FragmentPagerAdapter {


        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return corresponding fragment.

            Log.d(MY_TAG, "Position=" + position);

            switch (position) {
                case 0:
                    Log.d(MY_TAG, "case0=" + 0);
                    return MyOrdersFragment.newInstance();
                case 1:
                    Log.d(MY_TAG, "case1=" + 1);
                    return MyTripsFragment.newInstance();
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            Log.d(MY_TAG, "getcount");
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Log.d(MY_TAG, "getPageTite");
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    Log.d(MY_TAG, "case0");
                    return getString(R.string.my_orders);
                //return getString(R.string.i_trip).toUpperCase(l);
                case 1:
                    Log.d(MY_TAG, "case1");
                    return getString(R.string.my_trips);
                //return getString(R.string.i_want).toUpperCase(l);
            }
            return null;
        }


    }


}
