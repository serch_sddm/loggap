package com.sduran.loggap.Fragments;

import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sduran.loggap.R;
import com.sduran.loggap.activity.MainActivity;
import com.sduran.loggap.adapters.OrdersArrayAdapter;
import com.sduran.loggap.adapters.TravelersArrayAdapter;
import com.sduran.loggap.auth.AuthenticationManager;
import com.sduran.loggap.request.SearchMyOrdersRequest;
import com.sduran.loggap.request.SearchMyTravelsRequest;
import com.sduran.loggap.support.dto.rest.OrdersDTO;
import com.sduran.loggap.support.dto.rest.SearchMyOrdersRequestDTO;
import com.sduran.loggap.support.dto.rest.SearchMyOrdersResponseDTO;
import com.sduran.loggap.support.dto.rest.SearchMyTravelsRequestDTO;
import com.sduran.loggap.support.dto.rest.SearchMyTravelsResponseDTO;
import com.sduran.loggap.support.dto.rest.TravelsDTO;
import com.sduran.loggap.util.AlertDialogUtils;
import com.sduran.loggap.util.UIHelper;

import java.util.List;

import static com.sduran.loggap.support.dto.rest.BaseResponseDTO.REST_EXPIRED_CODE;
import static com.sduran.loggap.support.dto.rest.BaseResponseDTO.REST_SUCCESS_CODE;

/*
*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyTripsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyTripsFragment#newInstance} factory method to
 * create an instance of this fragment.
*/

public class MyTripsFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    LinearLayout progressView;
    ListView bodyView;
    TextView mFeedback;

    /*private OnFragmentInteractionListener mListener;*/

 /*
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PostTripFragment.
     */
    // TODO: Rename and change types and number of parameters
/*    public static PostTripFragment newInstance(String param1, String param2) {
        PostTripFragment fragment = new PostTripFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/
    public static MyTripsFragment newInstance() {
        MyTripsFragment fragment = new MyTripsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public MyTripsFragment() {
        // Required empty public constructor
    }

    /*   @Override
       public void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           setRetainInstance(true);
           if (getArguments() != null) {
               mParam1 = getArguments().getString(ARG_PARAM1);
               mParam2 = getArguments().getString(ARG_PARAM2);
           }
       }*/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_my_trips, container, false);


        progressView = (LinearLayout) v.findViewById(R.id.my_travels_progress_form);
        bodyView = (ListView) v.findViewById(R.id.my_listview);
        mFeedback = (TextView) v.findViewById(R.id.feedback_message);

        showProgress(true);

        attemptSearch();


        return v;
    }

    private void attemptSearch() {
        String token = AuthenticationManager.getRESTAuthenticationToken(getContext());

        SearchMyTravelsRequest request = new SearchMyTravelsRequest(new SearchMyTravelsRequestDTO(token), getResources());
        getSpiceManager().execute(request, new SearchMyTravelsRequestListener());
    }

    public void showProgress(final boolean show) {
        UIHelper.showProgress(show, bodyView, progressView, getResources());
    }

    AlertDialog.Builder alertView;
    TravelersArrayAdapter adapter;

    private class SearchMyTravelsRequestListener implements RequestListener<SearchMyTravelsResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e("Error ", spiceException.getMessage());
            showProgress(false);
            /*Toast.makeText(getActivity(),
                    getString(R.string.error_tax_not_found), Toast.LENGTH_SHORT)
                    .show();
*/
            alertView = AlertDialogUtils.initAlertDialog(getContext(), getString(R.string.title_orders_incorrect), getString(R.string.description_orders_not_found), R.drawable.ic_dialog_alert_holo_light);
            alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertView.show();
        }

        @Override
        public void onRequestSuccess(SearchMyTravelsResponseDTO response) {

            showProgress(false);

            if(REST_SUCCESS_CODE == response.getCode()){

                //gets and sends the concepts retrieved from details
                List<TravelsDTO> items;
                items = response.getTravels();


                if(items.isEmpty()){
                    bodyView.setVisibility(View.GONE);
                    mFeedback.setVisibility(View.VISIBLE);
                }

                else {

                    adapter = new TravelersArrayAdapter(getActivity(),
                            android.R.layout.simple_list_item_1, items);
                    bodyView.setAdapter(adapter);
                }

            }else if (REST_EXPIRED_CODE == response.getCode()) { //token expired
                showProgress(false); // hide the loading progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(getActivity(), getString(R.string.feedback_warning), getString(R.string.error_token_expired)+""+getString(R.string.error_logout), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ((MainActivity)getActivity()).executeLogOut();


                    }
                });
                alertView.show();

            }else{
                Log.d("Error ", response.getMessage());

                /*Toast.makeText(getActivity(),
                        getString(R.string.error_tax_not_found), Toast.LENGTH_SHORT)
                        .show();*/

                /*alertView = AlertDialogUtils.initAlertDialog(getActivity(),getString(R.string.title_travels_incorrect), getString(R.string.description_orders_not_found), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertView.show();*/
                mFeedback.setVisibility(View.VISIBLE);
                bodyView.setVisibility(View.GONE);
            }
        }
    }

    /*// TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    *//**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *//*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
*/
}
